#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <string>
#include <iostream>

#include "topconn_component.hpp"
#include "protocal/sftp/sftpclient.hpp"

using namespace std;
using namespace topconn;

string genBody()
{
    string body;
    body.append("<!DOCTYPE html>");
    body.append("<html>");
    body.append("<head>");
    body.append("  <title>RESTful API demo</title>");
    body.append("</head>");
    body.append("<body>");
    body.append("    <h1>c++ httpserver demo</h1>");
    body.append("    <h2>simple http</h2>");
    body.append("    <h3>GET</h3>");
    body.append("    <div>");
    body.append("        <button id=\"btn\">get request</button>");
    body.append("    </div>");
    body.append("    <div>");
    body.append("     <label>Result1:</label> <span id=\"result1\">&nbsp;</span> ");
    body.append("    </div>");
    body.append("    <h3>POST</h3>");
    body.append("    <div>");
    body.append("      <label>Number 1:</label> <input type=\"text\" id=\"n1\" />  ");
    body.append("    </div>");
    body.append("    <div>");
    body.append("      <label>Number 2:</label> <input type=\"text\" id=\"n2\" />  ");
    body.append("    </div>");
    body.append("    <div>");
    body.append("     <label>Result2:</label> <span id=\"result2\">&nbsp;</span> ");
    body.append("    </div>");
    body.append("    <h2>websocket</h2>");
    body.append("    <div>");
    body.append("        <span id=\"ws_text\">&nbsp;</span>");
    body.append("        <br />");
    body.append("        <input type=\"text\" id=\"send_input\" />");
    body.append("        <button id=\"send_button\">Send</button>");
    body.append("    </div>");
    body.append("</body>");
    body.append("</html>");
    return body;
}

void tcp_test()
{
	string request, response;
	request = "我是测试报文，你收了没!!!";
	cout << "发送报文：" << request << endl;
	ConnComponent::sendPack("tcp_test1", request, response);
	cout << "接收报文1：" << response << endl;
}

void udp_test()
{
	string request, response;
	request = "我是测试报文，你收了没!!!";
	cout << "发送报文：" << request << endl;
	ConnComponent::sendPack("tcp_test2", request, response);
	cout << "接收报文2：" << response << endl;	
}

void http_test()
{
	string request("http_test.txt"), response;
	string path = "/mjyx-mall-gateway2/web/index.php/auth/login";
	ConnComponent::sendPack("tcp_test3",request,response,path);
	cout << "接收报文2：" << response << endl;	
}

void http_test2()
{
	string request(genBody()), response;
	string path = "/mjyx-mall-gateway2/web/index.php/auth/login";
	ConnComponent::sendPack("tcp_test4", request, response, path);
	cout << "接收报文2：" << response << endl;
}

void sftp_test()
{
#ifdef SSH_SFTP_SUPPORT
    SftpClient client;
    client.setAddr("192.168.147.128", 22);
    client.setUser("aips");
    client.setPass("aips");
    if (client.connect() != 0)
    {
        return;
    }
    client.putfile("/home/aips/bin/connector.xml", "/home/aips/bin/subdir/connector.xml");
#endif // SSH_SFTP_SUPPORT
}

int main(int argc, char* argv[])
{
	try
	{
		ConnComponent::setConnLoadMode(true);
		//tcp_test();
		//udp_test();
		//http_test();
		//http_test2();
        sftp_test();
	}
	catch (const std::exception& e)
	{
		cout << "error:" << e.what() << endl;
	}
	return 0;
}