#include <memory>

#include "topconn_exception.hpp"
#include "topconn_logutil.hpp"

#include "topconn_cfg.hpp"
#include "topconn_cfg_loader.hpp"
#include "topconn_connector.hpp"
#include "topconn_factory.hpp"

#include "topconn_processor.hpp"

namespace topconn
{
	int ConnProcessor::sendPack(const string& connectorId, const string& connPath, const string& request, string& response,const string& atx)
	{
		TLOG_DEBUG("Request data=[%s]", request.c_str());
		TLOG_INFO("ConnectorId=%s",connectorId.c_str());
		TLOG_INFO("ConnectorPath=%s",connPath.c_str());

		TopConnResource resource;
		ConnResourceLoader::LoaderResource(connPath,connectorId,resource);
		TLOG_DEBUG("Start applying %s communication protocol processing", resource.getProtocalCfg().getType().c_str());
        return ConnectorFactory::create(resource)->doExecute(request, response, atx);
	}

	int ConnProcessor::sendPack(const string& protocalType, map_t& params, const string& request, string& response,const string& atx)
	{
		TopConnResource resource(params);
		ConnResourceLoader::LoaderResource(protocalType,params,resource);

		TLOG_DEBUG("Start applying %s communication protocol processing", protocalType.c_str());

        return ConnectorFactory::create(resource)->doExecute(request, response, atx);
	}
};