#pragma once

#include <string>
#include <map>
#include <exception>
using namespace std;

namespace topconn
{
    class ConnComponent
    {
        using map_t = map<string, string>;
    public:
        static void setConnLoadMode(bool mode = true);
        static void setConnPath(const string& connPath);
        static int  sendPack(const string& connectorId, const string& request, string& response,const string& atx = string());
        static int  sendPack(const string& protocalType, map_t& params, const string& request, string& response,const string& atx = string());
    };
};