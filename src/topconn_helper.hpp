#pragma once

#include <string>
#include <vector>
#include <initializer_list>

using namespace std;

#include "topconn_cfg.hpp"
#include "topconn_exception.hpp"
#include "topconn_string.hpp"
#include "topconn_logutil.hpp"


namespace topconn
{
	class ConnectorHelper
	{
	public:
		static string getValue(AttributeableCfg& t, const string& attr, const string& defVal = string());
		static void checkValidRange(AttributeableCfg& e, const string& attr, std::initializer_list<std::string> allowValues);
		static void checkIntRange(AttributeableCfg& e, const string& attr, int min, int max);
		static void checkBooleanValue(AttributeableCfg& e, const string& attr);
	private:
		static string getName(AttributeableCfg& e);
	};
};