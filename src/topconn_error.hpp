#pragma once

namespace topconn
{
    typedef enum
    {
        SUCESSED = 0,
        INVALID_SOCKET,
        CREATE_SOCKET_ERROR,
        CONNECT_ERROR,
        CONNECT_TIMEOUT,
        NOT_CONNECT,
        RECV_TIMEOUT,
        RECV_ERROR,
        SEND_ERROR,
        PEER_CLOSE,
        SOCKETOPT_ERROR,
        ALLOC_ERROR,
        RCVDATA_INVALID,
        SSL_NOTSUPPORT_ERROR,
        SSL_INIT_ERROR,
        SSL_CONNECT_ERROR,
        SSL_LOAD_CERT_ERROR,
        SSL_RECV_ERROR,
        SSL_SEND_ERROR,
        SSL_TIMEOUT_ERROR,
        INVALID_HANDLE,
        OTHER_ERROR
    }TOPCONN_ERROR;

    /** �������ӳ���ϵ�� **/
#define TCPCONN_ERRNO_MAP(XX)  \
        XX(SUCESSED,         "SUCESSED")   \
        XX(INVALID_SOCKET,   "INVALID_SOCKET")  \
        XX(CREATE_SOCKET_ERROR,"CREATE_SOCKET_ERROR")  \
        XX(CONNECT_ERROR,    "CONNECT_ERROR")  \
        XX(CONNECT_TIMEOUT,  "CONNECT_TIMEOUT")  \
        XX(NOT_CONNECT,      "NOT_CONNECT")  \
        XX(RECV_TIMEOUT,     "RECV_TIMEOUT")  \
        XX(RECV_ERROR,       "RECV_ERROR")  \
        XX(SEND_ERROR,       "SEND_ERROR" )  \
        XX(PEER_CLOSE,       "PEER_CLOSE" )  \
        XX(SOCKETOPT_ERROR,  "SOCKETOPT_ERROR")  \
        XX(ALLOC_ERROR,      "ALLOC_ERROR")  \
        XX(RCVDATA_INVALID,  "RCVDATA_INVALID")  \
        XX(SSL_NOTSUPPORT_ERROR, "SSL_NOTSUPPORT_ERROR") \
        XX(SSL_INIT_ERROR,    "SSL_INIT_ERROR")  \
        XX(SSL_CONNECT_ERROR, "SSL_CONNECT_ERROR") \
        XX(SSL_LOAD_CERT_ERROR,"SSL_LOAD_CERT_ERROR")  \
        XX(SSL_RECV_ERROR,    "SSL_RECV_ERROR")  \
        XX(SSL_SEND_ERROR,    "SSL_SEND_ERROR")    \
        XX(SSL_TIMEOUT_ERROR, "SSL_TIMEOUT_ERROR") \
        XX(INVALID_HANDLE, "INVALID_HANDLE") \
        XX(OTHER_ERROR,  "OTHER_ERROR")


#define TOPCONN_STRERROR(CODE,DESC) case CODE : return DESC;    

    inline const char* topconn_strerror(int code)
    {
        switch (code) {
            TCPCONN_ERRNO_MAP(TOPCONN_STRERROR)
        default:  return "unkown other error";
        }
    };
};