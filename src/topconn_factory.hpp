#pragma once

#include <string>
#include <cstdlib>
#include <exception>
#include <memory>

using namespace std;

#include "topconn_logutil.hpp"
#include "topconn_cfg.hpp"

namespace topconn
{
	class Connector;
	typedef std::shared_ptr<Connector>  ConnectorPtr_t;

	class ProtocalType
	{
	public:
		static int getProtocalType(const string& protocalType);
		static string getProtocalType(int protocalType);
	};

	class ConnectorFactory
	{
	public:
		static ConnectorPtr_t create(TopConnResource& resource);
	};
};


