#pragma once

#include <string>
#include <exception>

using namespace std;

namespace topconn
{
    class ConnException : public exception
    {
    protected:
        int m_erorcd;
        string m_erortx;
    public:
        ConnException()
            : m_erorcd(0)
        {
        }

        ConnException(const string& error)
            : m_erorcd(0)
            , m_erortx(error)
        {
        }

        ConnException(int erorcd,const string& error)
            : m_erorcd(erorcd)
            , m_erortx(error)
        {
        }

        virtual ~ConnException() {};

        virtual const char* what() const noexcept
        {
            return m_erortx.c_str();
        }

        virtual const int error() const noexcept
        {
            return m_erorcd;
        }
    };

    class NullPointerException : public ConnException
    {
    public:
        NullPointerException(const string& error)
            : ConnException(string("空指针异常:") + error)
        {
        }
    };

    class PolicyUnsupportedException : public ConnException
    {
    public:
        PolicyUnsupportedException(const string& error)
            : ConnException(string("策略协议不被支持:") + error)
        {
        }
    };

    class ConnectTimeoutException : public ConnException
    {
    public:
        ConnectTimeoutException(const string& error)
            : ConnException(error)
        {
        }
    };

    class RecvTimeoutException : public ConnException
    {
    public:
        RecvTimeoutException(const string& error)
            : ConnException(error)
        {
        }
    };

};