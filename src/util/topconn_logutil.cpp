
#include <time.h>
#include <errno.h>

#include "topconn_string.hpp"
#include "topconn_logutil.hpp"

namespace topconn
{
    #define maxLogFileSize  30
    static const char* loglevel_strs[] = {"TRACE","DEBUG","INFO","WARN","ERROR"};

	static struct
	{
		char sLogLevel[32];
		eLogLevel_t xLogLevel;
	} f_tLogLevelMaps[] = {
		{"TRACE",  L_TRACE},
		{"DEBUG",  L_DEBUG},
		{"INFO"	,  L_INFO},
		{"WARN"	,  L_WARN},
		{"ERROR",  L_ERROR}
	};    

    topconn_log* topconn_log::m_instance = nullptr;
    std::mutex topconn_log::m_lock;
    bool topconn_log::m_status = false;

    char* topconn_gettime(char* datetime)
    {
        time_t 	now_time;
        struct tm* now_tm;
        now_time = time((time_t*)0);
        now_tm = localtime(&now_time);
        strftime(datetime, 16, "%Y%m%d%H%M%S", now_tm);
        return datetime;
    }

    long  topconn_getfilesize(const char* psFileName)
    {
        long nFileLen = 0;
#ifdef WIN32
        FILE* hFile = nullptr;
        fopen_s(&hFile,psFileName, "r");
        if (!hFile)
        {
            printf("打开文件失败，文件不存在[%s]！\n", psFileName);
            return -1;
        }
        fseek(hFile, 0, SEEK_END);
        nFileLen = ftell(hFile);
        fclose(hFile);
#else        
        struct stat  f_stat;
        if (access(psFileName, 0) < 0)  return 0;
        if (stat(psFileName, &f_stat) == -1)
        {
            printf("stat [%s] fail,errno[%d]\n", psFileName, errno);
            return -1;
        }
       nFileLen = (long)f_stat.st_size;
#endif     
        return nFileLen;
    }    

    long  topconn_getfilesize(FILE* hFile)
    {
        if(hFile == NULL) return -1;
        long nCurPos = ftell(hFile);
        fseek(hFile, 0, SEEK_END);
        long nFileLen = ftell(hFile);
        fseek(hFile, nCurPos, SEEK_SET);
        return nFileLen;
    }  

    bool topconn_exists_file(const char* psFileName)
    {
    #ifdef WIN32
        return (_access(psFileName, 0) == 0) ? true : false;
    #else
        return (access(psFileName, 0) == 0) ? true : false;
    #endif
    }    

	static eLogLevel_t topconn_loglevl_cvt(const string& sLogLevel)
	{
		eLogLevel_t _LogLevel= eLogLevel_t::L_INFO;
		for(uint16_t i=0;i < sizeof(f_tLogLevelMaps)/sizeof(f_tLogLevelMaps[0]);i++)
		{
			if(sLogLevel==f_tLogLevelMaps[i].sLogLevel)
			{
				_LogLevel=f_tLogLevelMaps[i].xLogLevel;
				break;
			}
		}
		return _LogLevel;
	}    

    int topconn_log::openlog()
    {					
        if(topconn_log::m_status)
        {
            return 0;
        }

        char topconn_logpath_debug[256] = {0};
        char topconn_logpath_error[256] = {0};
        
        sprintf(topconn_logpath_debug,"%s%clog%c%s",
            getenv(USERHOME),PATH_DELIMITER,PATH_DELIMITER,TOPCONN_DEBUG_LOG);

        m_fpDebug = fopen(topconn_logpath_debug,"ab+");
        if(!m_fpDebug)  return -1;
        
        topconn_log::m_status = true;
        return 0;
    }

    void topconn_log::close()
    {
        if(m_fpDebug)
        {
            fclose(m_fpDebug);
            m_fpDebug = NULL;
        }
        topconn_log::m_status = false;
    }

    void  topconn_log::writelog(const char* file,const char* function,int line,int level,const char* fmt,...) 
    {
        char datetime [32] = {0}; 
        char logtext[8192]={0},logfilename[128]={0},prtbuf[8192]={0};

        /// @brief  获取环境变量及系统时间
        char* psEnv = getenv(TOPCONN_DEUBG_ENV_TAG);
        int dbsLevel = psEnv ? topconn_loglevl_cvt(psEnv) : L_DEBUG;
        topconn_gettime(datetime);

        /// @brief  格式化日志内容
        /// 日志时间|日志级别|进程ID|文件名|行号|日志内容
        va_list ap;
        va_start(ap, fmt);
        vsnprintf(logtext, sizeof(logtext)-1, fmt, ap);
        va_end(ap);
        snprintf(
            prtbuf,
            sizeof(prtbuf)-1,
            "%s:%5s:%8d:%23.23s:%4d: %s\n",
            datetime, 
            loglevel_strs[level],
            getpid(), 
            BASENAME(file), 
            line,
            logtext
        );
        /// @brief  写日志文件
        writelog(dbsLevel,level,prtbuf);	
    }

    void topconn_log::writelog(int dbsLevel,int level,const char* data)
    {
#ifdef _DEBUG_
        {
            //输出到控制台
            printf("%s", data);
        }
#endif
        if(m_fpDebug && level >= dbsLevel)
        {
            fprintf(m_fpDebug,"%s",data);
            fflush(m_fpDebug);	
        }
        rotate();
    }

    void topconn_log::rotate()
    {
        int iRet = 0;
        if(m_fpDebug)
        {
            if(topconn_getfilesize(m_fpDebug) > maxLogFileSize*1024*1024)
            {                
                char bakfile1[256]={0};
                char bakfile2[256]={0};
                char bakfile3[256]={0};

                char topconn_logfile[256] = {0};
                sprintf(topconn_logfile,"%s%clog%c%s",
                    getenv(USERHOME),PATH_DELIMITER,PATH_DELIMITER,TOPCONN_DEBUG_LOG);

                sprintf(bakfile1,"%s%clog%c%s.bak1",
                    getenv(USERHOME),PATH_DELIMITER,PATH_DELIMITER,TOPCONN_DEBUG_LOG);
                sprintf(bakfile2,"%s%clog%c%s.bak2",
                    getenv(USERHOME),PATH_DELIMITER,PATH_DELIMITER,TOPCONN_DEBUG_LOG);
                sprintf(bakfile3,"%s%clog%c%s.bak3",
                    getenv(USERHOME),PATH_DELIMITER,PATH_DELIMITER,TOPCONN_DEBUG_LOG);

                std::lock_guard<std::mutex> _lock(topconn_log::m_lock);
                long nLogFileSize = topconn_getfilesize(topconn_logfile);
                if(nLogFileSize > maxLogFileSize*1024*1024)
                {
                    if(topconn_exists_file(bakfile2))
                        iRet = rename( bakfile2, bakfile3 );
                    if(topconn_exists_file(bakfile1))
                        iRet = rename( bakfile1, bakfile2 );
                    iRet = rename( topconn_logfile, bakfile1 );
                }
                close();
                openlog();
            }
        }
    }

    topconn_log* topconn_log::rootLog()
    {
        if (topconn_log::m_instance==nullptr)
        {
            std::lock_guard<std::mutex> _lock(topconn_log::m_lock);
            if(topconn_log::m_instance==nullptr)
            {
                topconn_log::m_instance = new topconn_log();
                topconn_log::m_instance->openlog();
            }
        }
        return topconn_log::m_instance;				
    };	
}

