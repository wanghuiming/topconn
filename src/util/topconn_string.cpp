#include "topconn_string.hpp"

#include <functional>
#include <algorithm>

namespace topconn
{
	const int maxBufSize = 2048 ;

	string& topconn_string::to_upper(string &sstring)
	{
		transform(sstring.begin(),sstring.end(),sstring.begin(),::toupper);
		return sstring;
	}

	string& topconn_string::to_lower(string &sstring)
	{
		transform(sstring.begin(),sstring.end(),sstring.begin(),::tolower);
		return sstring;
	}

	string& topconn_string::ltrim(string &sstring)
	{
		if (sstring.empty()) return sstring;
#if 0
		//string::iterator p = find_if(sstring.begin(), sstring.end(), not1(ptr_fun<int, int>(isspace)));
		//sstring.erase(sstring.begin(), p);
		sstring.erase(sstring.begin(), std::find_if(sstring.begin(), sstring.end(), [](int c) {
			return !std::isspace(c); })
		);
#endif
		sstring.erase(0,sstring.find_first_not_of("\t "));
		return sstring;
	}

	string& topconn_string::rtrim(string &sstring)
	{
		if (sstring.empty()) return sstring;
#if 0
		//string::reverse_iterator p = find_if(sstring.rbegin(), sstring.rend(), not1(ptr_fun<int , int>(isspace)));
		//sstring.erase(p.base(), sstring.end());
		sstring.erase(std::find_if(sstring.rbegin(), sstring.rend(), [](int c) {
			return !std::isspace(c); }).base(),sstring.end()
		);
#endif
		sstring.erase(sstring.find_last_not_of("\t ") + 1);
		return sstring;
	}

	string& topconn_string::trim(string& sstring)
	{
		topconn_string::ltrim(sstring);
		topconn_string::rtrim(sstring);
		return sstring;
	}

	char* topconn_string::delspace( char* sstring  )
	{
		uint32_t ii, ij;

		if(sstring == NULL) return NULL;
		char* pBuf = strdup(sstring); 

		ii = strlen(sstring);
		for ( ; ii>0; ii-- ){
			if ( pBuf[ii-1] == ' ' || pBuf[ii-1] == '\t' || pBuf[ii-1] == '\n'  || pBuf[ii-1] == '\r' )
				pBuf[ii-1] = '\0' ;
			else
				break;
		}

		for ( ij=0; ij<ii; ij++ ){
			if ( pBuf[ij] != ' '  && pBuf[ij] != '\t' && pBuf[ij] != '\n' && pBuf[ij] != '\r' )
				break;
		}

		strcpy( sstring, pBuf+ij );	
		if( pBuf)	free(pBuf);
		return sstring;
	}

	size_t topconn_string::count_gbchars(const string& sstring)
	{
		size_t count = 0;
		for(uint32_t i=0; i < sstring.size(); i++)
		{
			unsigned char ch = sstring[i];
			if(ch > 0x80) 
				i++;
			count ++;
		}
		return count;
	}		

	string& topconn_string::format(string& sstring,const char * psFmt, ...)
	{
		va_list args;
		va_start(args, psFmt);
		{
			char buffer[maxBufSize+1];
			vsnprintf(buffer, maxBufSize, psFmt, args);
			sstring.assign(buffer);
		}
		va_end(args);
		return sstring;
	}

	string topconn_string::strcat(std::initializer_list<std::string> values,const string& sep)
	{
		string sstring;
		for (auto& value : values)
		{
			sstring.append(value).append(sep);
		}
		return sstring;
	}

	string& topconn_string::fillchar_left(string& sstring, uint32_t length, char padChar)
	{
		string strTmpBuf("");
		uint32_t strLen = sstring.length();
		if(strLen < length){
			uint32_t padLen = length - strLen;
			for(uint32_t i=0;i< padLen;i++)
				strTmpBuf = strTmpBuf+padChar;
			return sstring.insert(0,strTmpBuf);
		}
		else
		{
			// 长度超过指定长度，截取
			if (strLen > length)
			{
				sstring = sstring.substr(0,length);
			}
		}
		return sstring;
	}

	string& topconn_string::fillchar_right(string& sstring, uint32_t length, char padChar)
	{
		string strTmpBuf("");
		uint32_t strLen = sstring.length();
		if(strLen < length)
		{
			uint32_t padLen = length - strLen;
			for(uint32_t i=0;i< padLen;i++)
				strTmpBuf = strTmpBuf+padChar;
			return sstring.append(strTmpBuf);
		}
		else
		{
			// 长度超过指定长度，截取
			if (strLen > length)
			{
				sstring = sstring.substr(0, length);
			}
		}
		return sstring;
	}

	string& topconn_string::removechar_right(string& sstring,char ch)
	{
		if (sstring.empty() || sstring.back() != ch)
		{
			return sstring;
		}
		while (sstring.size() > 0 )
		{
			if (sstring.back() != ch)
			{
				break;
			}
			sstring.pop_back();
		}
		return sstring;
	}

	string& topconn_string::removechar_left(string& sstring, char ch)
	{
		if (sstring.empty() || sstring.front() != ch)
		{
			return sstring;
		}
		while (sstring.size() > 0)
		{
			if (sstring.front() != ch)
			{
				break;
			}
			sstring.erase(0,1);
		}
		return sstring;
	}

	bool topconn_string::isnumber(const string& sstring)
	{
		stringstream sin(sstring);
		double d;
		char c;
		if(!(sin >> d)) return false;
		if(sin >> c)    return false;
		return true;
	}

	bool topconn_string::isalpha(const string& sstring)
	{
		if(sstring.empty())
			return false;
		for(auto& c : sstring)
		{
			if(::isalpha((int)c))
				continue;
			else
				return false;
		}
		return true;		
	}	

	bool topconn_string::isalpha_or_num(const string& sstring)
	{
		if(sstring.empty())
			return false;
		for(auto& c : sstring)
		{
			if(::isalnum((int)c))
				continue;
			else
				return false;
		}	
		return true;
	}	

	bool topconn_string::isalpha_and_num(const string& sstring)
	{
		if(sstring.empty()) return false;
			
		bool is_alpha = false,is_number = false;
		for(auto& c : sstring)	
		{
			if(!::isalpha(int(c)) && !::isdigit((int)c) )
				return false;
				
			if(::isalpha((int)c))
				is_alpha = true;
			if(::isdigit((int)c))
				is_number = true;		
		}
		return is_alpha && is_number ? true : false;
	}

	void topconn_string::fillspace(void* text,size_t len)
	{
		char* p = (char*)text;
		for(uint32_t i=0;i<len;i++) 
		{
			if(p[i]==0x00) p[i]=0x20; 
		}
	}

	string topconn_string::random(int n)
	{
		const char sFormat[] = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ";
		size_t len = strlen(sFormat);
		
		srand(time(0));
		
		string sRandom;
		for (int i = 0;i < n; i++)
		{
			int j = rand()%len;
			sRandom.append(1,sFormat[j]);
		}
		return sRandom;
	}	

	unsigned char hexchar(char cAscii)
	{
		typedef struct _CHAR2DECIMAL {
			char c;
			unsigned char bValue;
		}CHAR2DECIMAL, *PCHAR2DECIMAL;

		const CHAR2DECIMAL c2a[22] = {
			{'0', 0},{'1', 1},
			{'2', 2},{'3', 3},
			{'4', 4},{'5', 5},
			{'6', 6},{'7', 7},
			{'8', 8},{'9', 9},
			{'a', 10},{'A', 10},
			{'b', 11},{'B', 11},
			{'c', 12},{'C', 12},
			{'d', 13},{'D', 13},
			{'e', 14},{'E', 14},
			{'f', 15},{'F', 15}
		};
		unsigned char uch = 0;
		for (uint16_t i = 0 ; i < 22; i++) {
			if (c2a[i].c == cAscii)
			return c2a[i].bValue;
		}
		return uch;
	}

	string topconn_string::str2hex(const string& sstring)
	{
		string rs_string("");
		for(auto& c : sstring)
		{
			char buf[3] = {0};
			sprintf(buf, "%02X", (unsigned char)c);
			rs_string.append(buf);
		}
		return rs_string;
	}

	string topconn_string::hex2str(const string& sstring)
	{
		string rs_string("");
		unsigned char dest[maxBufSize]={0};

		uint32_t hexLen = sstring.length();
		if (hexLen == 0 || hexLen % 2 != 0)
			return rs_string;

		uint32_t index=0;
		uint32_t dstLen = sizeof(dest);
		for (uint32_t i = 0; i < hexLen; i++)
		{
			if (index > dstLen) return "";
			dest[index] = hexchar(sstring[i]) * 16;
			dest[index] += hexchar(sstring[++i]);
			index++;
		}
		rs_string = string((char*)dest,strlen((char*)dest));
		return rs_string;
	}	

	std::vector<string> topconn_string::split(const string& c)
	{
		std::vector<string> result;
		std::istringstream sin(c);
		string word;
		while(sin>>word) result.push_back(word);
		return result;
	}

	std::vector<string> topconn_string::split(const string& c,const string& delim)
	{
		std::vector<string> rs_strings;	
		if(c.size() == 0) 
		{
			return rs_strings;
		}

		string sstring(c);	
		string::size_type pos;
		if(sstring.compare(sstring.size()-delim.size(), delim.size(), delim) != 0)
		{
			sstring+=delim;
		}
			
		uint32_t size=sstring.size();
		for(uint32_t i=0; i<size; i++)
		{
			pos=sstring.find(delim,i);
			if( pos < size) 
			{
				string s=sstring.substr(i,pos-i);
				rs_strings.push_back(s);
				i=pos+delim.size()-1;
			}
		}
		return rs_strings;
	}

	std::vector<string> topconn_string::split_chs(string sstring,const string& delim)
	{
		std::vector<string> rs_strings;

		if(sstring.empty())
		{
			return rs_strings;
		}

		if(sstring.compare(sstring.length()-delim.length(), delim.length(), delim ) != 0)
		{
			sstring+=delim;
		}

		size_t start_pos=0;
		size_t pos = 0;
		for(uint32_t i=0; i<sstring.size(); i++)
		{
			unsigned char ch = sstring[i];
			if(ch > 0x80){
				i++;
				continue;
			}
			if( sstring.compare(i,delim.size(),delim) != 0)
				continue;

			pos = i;
			string s=sstring.substr(start_pos,pos-start_pos);
			rs_strings.push_back(s);
			start_pos=pos+delim.size();
		}
		return rs_strings;
	}		

	bool topconn_string::readini(const string& ss,string& key,string& value)
	{
		if(ss.front() == '#')
		{
			return false;
		}
		std::string::size_type find = ss.find("=");
		if(find != std::string::npos)
		{
			key = ss.substr(0,find);
			value = ss.substr(find+1);
			ltrim(value);
			rtrim(key);
			return true;
		}	
		return false;
	}	

	bool topconn_string::is_utf8(const char* c)
	{
		//UFT8可用1-6个字节编码,ASCII用一个字节
		unsigned int nBytes = 0;
		unsigned char chr;
		bool bAllAscii = true;
		for (unsigned int i = 0; c[i] != '\0'; ++i)
		{
			chr = *(c + i);
			//判断是否ASCII编码,如果不是,说明有可能是UTF8,ASCII用7位编码,最高位标记为0,0xxxxxxx
			if (nBytes == 0 && (chr & 0x80) != 0)
			{
				bAllAscii = false;
			}
			if (nBytes == 0)
			{
				//如果不是ASCII码,应该是多字节符,计算字节数
				if (chr >= 0x80) {
					if (chr >= 0xFC && chr <= 0xFD) nBytes = 6;
					else if (chr >= 0xF8) nBytes = 5;
					else if (chr >= 0xF0) nBytes = 4;
					else if (chr >= 0xE0) nBytes = 3;
					else if (chr >= 0xC0) nBytes = 2;
					else return false;
					nBytes--;
				}
			}
			else{
				//多字节符的非首字节,应为 10xxxxxx
				if ((chr & 0xC0) != 0x80)
				{
					return false;
				}
				//减到为零为止
				nBytes--;
			}
		}

		//违返UTF8编码规则
		if (nBytes != 0) return false;
		//如果全部都是ASCII, 也是UTF8
		if (bAllAscii)   return true;
		return true;
	}

	bool topconn_string::is_gbk(const char* c)
	{
		//GBK可用1-2个字节编码,中文两个 ,英文一个
		unsigned int nBytes = 0;
		unsigned char chr;
		bool bAllAscii = true; //如果全部都是ASCII,
		for (unsigned int i = 0; c[i] != '\0'; ++i){
			chr = *(c + i);
			if ((chr & 0x80) != 0 && nBytes == 0) // 判断是否ASCII编码,如果不是,说明有可能是GBK
			{	
				bAllAscii = false;
			}
			if (nBytes == 0)
			{
				if (chr >= 0x80) {
					if (chr >= 0x81 && chr <= 0xFE){
						nBytes = +2;
					}
					else{
						return false;
					}
					nBytes--;
				}
			}
			else
			{
				if (chr < 0x40 || chr>0xFE)
				{
					return false;
				}
				nBytes--;
			}//else end
		}
		if (nBytes != 0)  	//违返规则
			return false;
		if (bAllAscii) //如果全部都是ASCII, 也是GBK
			return true;
		return true;
	}

	/**************************************16进制格式化处理***********************************/
	#define HEXLEN      20   /* 十六进制日志字符数 */
	struct  HEXBUF
	{
	int   point;           /* 数据显示位置 */
	int   chn  ;           /* 汉字标识     */
	char  scop[ 6  + 1 ];  /* 位置偏移数   */
	char  sstr[ 20 + 1 ];  /* ASCII 打印串 */
	char  shex[ 61 + 1 ];  /* 十六进制打印串 */
	} ;
	
	/*
	** 十六进制打印缓冲区初始化
	*/
	static void  HexBuf_init(struct HEXBUF  *p )
	{
		p->point = 0;
		memset(p->scop, 0x0 , sizeof(p->scop));
		memset(p->sstr, 0x20, HEXLEN );
		memset(p->shex, 0x20, HEXLEN*3 );
		return ;
	}

	/*
	** 定长数据包 十六进制格式化打印输出
	*/
	string topconn_string::FormatOutHex( const void *PInfo, const char *Title, int mLen)
	{
		int   cnt;        // 当前数据处理位置
		char  temp[5];
		unsigned char  *ptr =  (unsigned  char *) PInfo ;
		struct HEXBUF   hex;
		memset(&hex, 0x0, sizeof(hex));
		char sHexBuf[8192]={0};

		if(PInfo == NULL) return 0;
		strcpy(sHexBuf,"\n");
		sprintf( sHexBuf+strlen(sHexBuf),"%25s------- 十六进制格式化输出开始 长度(%d) -------\n", Title, mLen);
		HexBuf_init(&hex);
		for( cnt = 0; cnt < mLen ; cnt ++, ptr ++ )
		{
			int   point ;
			if(hex.point && (cnt % HEXLEN) == 0 ) 
			{
				hex.chn = hex.chn & 0x01;
				if( hex.chn ) 
					hex.sstr[HEXLEN-1]='.';
				sprintf(sHexBuf+strlen(sHexBuf),"%s: %s | %s |\n", hex.scop, hex.shex, hex.sstr);
				HexBuf_init( &hex );
			}
			if(hex.point == 0 )
				sprintf(hex.scop, "%04d", cnt );
			point = hex.point;
			if( (*ptr) < 0x20 ) 
				hex.sstr[point] = '.';
			else 
			{
				if(point == 0 && hex.chn ) 
				{
					hex.chn = 0;
					hex.sstr[0] = '.';
				}
				else 
				{
					hex.sstr[point] = (*ptr);
					if( (*ptr & 0x80) == 0x80 ) hex.chn ++ ;
				}
			}
			point *= 3;
			sprintf(temp, "%02x ", (*ptr & 0xff ) );
			memcpy(hex.shex + point , temp, 3);
			hex.point ++;
		}
		if( hex.point )
		{
			sprintf(sHexBuf+strlen(sHexBuf),"%s: %s | %s |\n", hex.scop, hex.shex, hex.sstr);
		}
		sprintf(sHexBuf+strlen(sHexBuf),"%25s------- 十六进制格式化输出结束 ------- ", Title);
		return string(sHexBuf);
	}
}
