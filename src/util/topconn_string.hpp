#pragma once

#include <cstring>
#include <string>
#include <vector>
#include <sstream>
#include <stdarg.h>
#include <initializer_list>

using namespace std;

namespace topconn
{
    class topconn_string
    {
    public:
        static string& to_upper(string &sstring);
        static string& to_lower(string &sstring);
        static string& ltrim(string &sstring);
        static string& rtrim(string &sstring);
        static string& trim(string& sstring);
        static char*   delspace( char* sstring);
        static size_t  count_gbchars(const string& sstring);
        static string& format(string& sstring,const char * psFmt, ...);
        static string strcat(std::initializer_list<std::string> values, const string& sep);
        static string& fillchar_left( string& sstring, uint32_t length, char padChar);	
        static string& fillchar_right(string& sstring, uint32_t length, char padChar);	  
        static string& removechar_right(string& sstring, char ch);
        static string& removechar_left(string& sstring, char ch);
        static bool isnumber(const string& sstring); 
        static bool isalpha(const string& sstring);
        static bool isalpha_or_num(const string& sstring);
        static bool isalpha_and_num(const string& sstring);
        static void fillspace(void* text,size_t len);
        static string random(int n);
        static string str2hex(const string& sstring);
        static string hex2str(const string& sstring);
        static std::vector<string> split(const string& c);     
        static std::vector<string> split(const string& c,const string& delim);   
        static std::vector<string> split_chs(string sstring,const string& delim);          

        /*base data type convert */
        template<class T1,class T2>
        static inline T2 convert(const T1& value)
        {
            stringstream sin;
            T2 result;
            sin<<value;        
            sin>>result;
            return result;
        }  
		
        /*base data type convert */
        template<class T1,class T2>
        static inline T2 convert(const T1& value, const T2& defvalue)
        {
            T2 result;          
            stringstream sin;
            sin << value;
            if(sin.str().empty())
                return defvalue;
            sin >> result;
            return result;
        }            

        template< class T>
        static inline string to_string(const T value)
        {
            return topconn_string::convert<T,std::string>(value);
        }

        template< class T>
        static int32_t to_int(const T& value)
        {
            return topconn_string::convert<T,int32_t>(value,0);
        }

        template< class T>
        static inline long to_long(const T& value)
        {
            return topconn_string::convert<T,long>(value,0);
        }

        template< class T>
        static inline unsigned long to_ulong(const T& value)
        {
            return topconn_string::convert<T,unsigned long>(value,0);
        }

        template< class T>
        static inline uint64_t to_uint64(const T& value)
        {
            return topconn_string::convert<T,uint64_t>(value,0);
        }

        template< class T>
        static inline double to_double(const T& value)
        {
            return topconn_string::convert<T,double>(value,0.00);
        } 

        static bool  readini(const string& ss,string& key,string& value);
        static bool  is_utf8(const char* c);
        static bool  is_gbk(const char* c);

        //格式化16进制输出
        static string FormatOutHex( const void *PInfo, const char *Title, int mLen);
    };
}
