#ifndef _TOPCONN_CROSS_PLATFORM_H_
#define _TOPCONN_CROSS_PLATFORM_H_

#ifdef WIN32
#	ifdef TOPCVT_EXPORTS
#		ifdef _WINDLL
#			define TOPCVT_API __declspec(dllexport)
#		else
#			define TOPCVT_API
#		endif
#	else
#		ifdef TOPCVT_IMPORT
#			define TOPCVT_API __declspec(dllimport)
#		else
#			define TOPCVT_API
#		endif
#	endif
#else
#	define TOPCVT_API
#endif


#ifdef WIN32
#include <Windows.h>
#include <io.h>
#else
#include <unistd.h>
#endif

#include <string.h>

#ifdef WIN32
#   pragma warning(disable:4100)
#   pragma warning(disable:4458)
#   pragma warning(disable:4996)
#	pragma warning(disable:4267)
#	pragma warning(disable:4244)
#   define PATH_DELIMITER '\\'
#   define USERHOME "USERPROFILE"
#else
#   define PATH_DELIMITER '/'
#   define USERHOME "HOME"
#endif

#ifndef BASENAME
#   define BASENAME(x) strrchr((x),PATH_DELIMITER)?strrchr((x),PATH_DELIMITER)+1:(x)
#endif

#ifdef _MSC_VER
    #define pid_t int
	#define lseek64 _lseeki64
    #define ftell64 _ftelli64
	#define strcasecmp _stricmp
	#define strncasecmp _strnicmp
    #define sleep(t) Sleep((t)*1000)
    #define access _access
    #define stat _stat 
#else
    #define lseek64 lseek
    #define ftell64 ftell
    #define SOCKET  int
#endif

#endif