#pragma once

#include "topconn_cfg.hpp"

namespace topconn
{
    class ConnProcessor
    {
        using map_t = map<string, string>;

    public:
        static int sendPack(const string& connectorId, const string& connPath,const string& request, string& response,const string& atx);
        static int sendPack(const string& protocalType, map_t& params, const string& request, string& response,const string& atx);
    };
};