#include "httpsconnector.hpp"
#include "topconn_exception.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void HttpsConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string url = cfg.get("url");
			if (url.empty()) {
				TLOG_ERROR("远程服务器Url不能为空");
				throw ConnException("远程服务器Url不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "maxRecvTimeout", 1, 300);
			ConnectorHelper::checkIntRange(cfg, "maxConnTimeout", 1, 30);
			ConnectorHelper::checkBooleanValue(cfg, "isBlock");
			ConnectorHelper::checkBooleanValue(cfg, "isIPV6");
			ConnectorHelper::checkBooleanValue(cfg, "isKeepalive");
			ConnectorHelper::checkValidRange(cfg, "method", {"post","get","put","delete"});
			ConnectorHelper::checkBooleanValue(cfg, "isVerify");
		}
	}

	void HttpsConnector::initConnect(ProtocalCfg& cfg)
	{
		string url = ConnectorHelper::getValue(cfg, "url");
		string maxRecvTimeout = ConnectorHelper::getValue(cfg, "maxRecvTimeout", "30");
		string maxConnTimeout = ConnectorHelper::getValue(cfg, "maxConnTimeout", "5");
		string isKeepalive = ConnectorHelper::getValue(cfg, "isKeepalive", "true");
		string isIPV6 = ConnectorHelper::getValue(cfg, "isIPV6", "false");
		string isVerify = ConnectorHelper::getValue(cfg, "isVerify", "false");
		string ca_cert_path = ConnectorHelper::getValue(cfg, "ca_cert_path");

		m_client.setUrl(url);
		m_client.setMaxConnTimeout(topconn_string::to_int<string>(maxConnTimeout));
		m_client.setMaxTimeout(topconn_string::to_int<string>(maxRecvTimeout));
		m_client.setCaCertPath(ca_cert_path);

		if (!isKeepalive.empty() && strcasecmp(isKeepalive.c_str(), "true") == 0)
			m_client.setKeepalive(true);
		if (!isIPV6.empty() && strcasecmp(isIPV6.c_str(), "true") == 0)
			m_client.setIpv6(true);
		if (!isVerify.empty() && strcasecmp(isVerify.c_str(), "true") == 0)
			m_client.setCertificateVerify(true);
		m_client.initClient();
	}

	int  HttpsConnector::execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx)
	{
		int iRet = 0;
		string path = atx;
		string method = ConnectorHelper::getValue(cfg,"method","post");
		string content_type = ConnectorHelper::getValue(cfg, "Content-type", "text/plain");

		TLOG_DEBUG("url=%s method=%s Content-type=%s", m_client.getUrl().c_str(),method.c_str(),content_type.c_str());

		iRet = m_client.request(method,path,request, content_type,resopnse);
		if (iRet != SUCESSED)
		{
			if (iRet == CONNECT_TIMEOUT)
			{
				throw ConnectTimeoutException("与远程服务器建立连接超时");
			}
			TLOG_ERROR("executor error:%s",topconn_strerror(iRet));
			throw ConnException("发送HTTPS请求失败："+ string(topconn_strerror(iRet)));
		}
		return SUCESSED;
	}
};