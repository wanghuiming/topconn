#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"
#include "httpsclient.hpp"

namespace topconn
{
	class HttpsConnector : public Connector
	{
	protected:
		HttpsClient m_client;
	public:
		HttpsConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};