#pragma once

#include <string>
#include <memory>
using namespace std;

#include "protocal/http/httpclient.hpp"

namespace httplib
{
	class SSLClient;
};

namespace topconn
{
	typedef std::shared_ptr<httplib::SSLClient>  HttpsClientPtr_t;

	class HttpsClient : public HttpClient
	{
	private:
		HttpsClientPtr_t m_httpsClientPtr;
		string m_caCertPath;
		bool m_isVerify;
	public:
		HttpsClient();
		HttpsClient(const string& url);
		~HttpsClient();

		void setCaCertPath(const string& certPath);
		void setCertificateVerify(bool on);

		const string& getCaCertPath()const;
		bool isCertificateVerify()const;

		virtual int initClient() override;
		virtual int request(const string& method, const string& path, const string& body, const string& content_type, string& response)  override;
	};
};