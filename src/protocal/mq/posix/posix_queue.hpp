#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <mqueue.h>
#include <unistd.h>
#include <string.h>

namespace topconn
{
	enum PosixMQOpType { POSIX_STAT = 0, POSIX_SET, POSIX_RMID };

#define POSIX_MAX_MQPATH_SIZE  128
#define POSIX_DEFAULT_MAXMSG  10
#define POSIX_DEFAULT_MSGSIZE 8192

	class CPosixQueue
	{
	private:
		mqd_t m_mqid;
		mq_attr m_attr;
		char m_mqpath[POSIX_MAX_MQPATH_SIZE];
		int m_timeout;
	public:
		CPosixQueue();
		CPosixQueue(mqd_t msqid);
		virtual ~CPosixQueue();

		inline void setTimeout(int timeout) { m_timeout = timeout; };
		inline int  getMsqId() { return m_mqid; };
		inline void setMsqId(mqd_t msqid) { m_mqid = msqid; };
		inline char* getQueueName() { return m_mqpath; };
		void setQsize(size_t queueSize);
		void setMsize(size_t msgSize);

		int  Msgget(const char* mqpath, bool isUnlink = false);
		int  Msgsnd(const void* sndbuf, int sndsize, bool isBlock = true);
		int  Msgrcv(char* rcvbuf, bool isBlock = true);

	public:
		static int MsgCtl(int msqid, int cmd, void* pArgs = NULL);
		static int MsgDepth(int msqid);
	};
};