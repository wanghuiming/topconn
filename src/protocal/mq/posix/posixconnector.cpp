#include "posixconnector.hpp"
#include "topconn_exception.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void PosixConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string mqpath = cfg.get("mqpath");
			if (mqpath.empty())
			{
				TLOG_ERROR("mqpath不能为空");
				throw ConnException("mqpath不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "maxRecvTimeout", 1, 300);
			ConnectorHelper::checkBooleanValue(cfg, "isBlock");
		}
	}

	void PosixConnector::initConnect(ProtocalCfg& cfg)
	{
		string mqpath = ConnectorHelper::getValue(cfg, "mqpath");
		if (m_client.Msgget(mqpath.c_str()) != 0)
		{
			throw ConnException("Msgget error:"+string(strerror(errno)));
		}
	}

	int  PosixConnector::execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx)
	{
		int iRet = 0;
		bool block = false;
		string mqpath = ConnectorHelper::getValue(cfg,"mqpath");
		TLOG_DEBUG("mqpath=[%s]", mqpath.c_str());

		iRet = m_client.Msgsnd(request.c_str(), block);
		if (iRet != 0)
		{
			throw ConnException("Msgsnd error:" + string(strerror(errno)));
		}
		return SUCESSED;
	}
};