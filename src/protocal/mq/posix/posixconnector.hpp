#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"

#include "posix_queue.hpp"

namespace topconn
{
	class PosixConnector : public Connector
	{
	protected:
		CPosixQueue m_client;
	public:
		PosixConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};