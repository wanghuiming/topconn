#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"

#include "mqmclient.hpp"

namespace topconn
{
	class MqmConnector : public Connector
	{
	protected:
		MqmClient m_client;
	public:
		MqmConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse, const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};