#pragma once

#include <string>
#include <unistd.h>
using namespace std;

#include "imqi.hpp"

namespace topconn
{
	class MqmClient
	{
	private:
		ImqQueueManager m_mqmManager;
		ImqChannel m_mqmChannel;
		string m_qmName;
		string m_channelName;
		string m_connectName;
		bool m_status;
		long m_expiry;
		int m_charset;
		long m_waitInterval;
		string m_loacalAddress;
		string m_userId;
		string m_passwd;

		ImqQueue m_mqmQueue;
		string m_qName;

		long m_reasonCode;
		long m_completionCode;
	public:
		MqmClient();
		MqmClient(const string& qmName);
		~MqmClient() {};

		long reasonCode();
		long completionCode();

		void setQmName(const string& qmName);
		void setQChannel(const string& qChannel);
		void setQConnectName(const string& qIp,const string& qPort);
		void setQConnectName(const string& qConnectName);
		void setLoacalAddress(const string& localAddress);
		void setExpire(long expire);
		void setWaitInterval(long waitInterval);
		void setUsereId(const string& userId);
		void setPassword(const string& password);
		void setQname(const string& qName);

		int init();
		int connect();
		int connect(const string& QMName, const string& QConnectName, const string& QChannelName);

		int currentDepth(const string& qName);
		int openQueue();

		int put(const string& message);
		int put(const string& qName, const string& message);
		int put(const string& qName, const string& message, const string& messageId);
		int put(const string& qName, const string& message, const string& messageId, const string& correlId);
		int put(const string& qName, ImqMessage& qMessage);

		int get(string& message);
		int get(const string& qName, string& message);
		int get(const string& qName, const string& messageId,string& message);
		int get(const string& qName, const string& correlId, string& message,string& messageId);
		int get(const string& qName, ImqMessage& qMessage, ImqGmo3& gmo);

		int browse(const string& qName, string& message);
		int browse(const string& qName, const string& messageId,string& message);
		int browse(const string& qName, ImqMessage& qMessage, ImqGmo3& gmo);

	private:
		void setReasonCode(ImqQueueManager& qmgr);
		void setReasonCode(ImqQueue& queue);
	};
};