#include "mqconnector.hpp"

#include "topconn_exception.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void MqmConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string qMgrName = cfg.get("qmgrName");
			string qConnectName = cfg.get("connectName");
			string qChannel = cfg.get("channel");
			string qSndName = cfg.get("qsName");
			string qRcvName = cfg.get("qrName");
			string qExpire = cfg.get("expire");
			string qWaitInterval = cfg.get("waitInterval");
			string qLoacalAddress = cfg.get("loacalAddress");
			string qCharset = cfg.get("charset");
			string qUserid = cfg.get("userid");
			string qPassword = cfg.get("password");

			if (qMgrName.empty())
			{
				TLOG_ERROR("队列管理器名称不能为空");
				throw ConnException("队列管理器名称不能为空");
			}
			if (qConnectName.empty())
			{
				TLOG_ERROR("连接名称不能为空");
				throw ConnException("连接名称不能为空");
			}
			if (qChannel.empty())
			{
				TLOG_ERROR("通道名不能为空");
				throw ConnException("通道名不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "expire", -1,100000);
			ConnectorHelper::checkIntRange(cfg, "waitInterval", -1, 100000);
		}
	}

	int  MqmConnector::execute(ProtocalCfg& cfg, const string& request, string& resopnse, const string& atx)
	{
		char sError[256] = { 0 };
		string qSndName = cfg.get("sendQueueName");
		string qRcvName = cfg.get("recvQueueName");

		if (m_client.put(qSndName,request) != 0)
		{
			sprintf(sError, "PUT消息至队列[%s]失败,reasonCode=%ld", qSndName.c_str(),m_client.reasonCode());
			TLOG_ERROR(sError);
			throw ConnException(sError);
		}
		return 0;
	}

	void MqmConnector::initConnect(ProtocalCfg& cfg)
	{
		char sError[256] = { 0 };
		string qMgrName = cfg.get("qmgrName");
		string qConnectName = cfg.get("connectName");
		string qChannel = cfg.get("channel");
		string qExpire = ConnectorHelper::getValue(cfg,"expire","-1");
		string qWaitInterval = ConnectorHelper::getValue(cfg, "waitInterval", "1");
		string qLoacalAddress = cfg.get("loacalAddress");
		string qCharset = cfg.get("charset");
		string qUserid = cfg.get("userid");
		string qPassword = cfg.get("password");

		TLOG_DEBUG("队列管理器：%s", qMgrName.c_str());
		TLOG_DEBUG("连接名称：%s", qConnectName.c_str());
		TLOG_DEBUG("通道名称：%s", qChannel.c_str());
		TLOG_TRACE("消息过期时间：%s", qExpire.c_str());
		TLOG_TRACE("消息等待时间：%s", qWaitInterval.c_str());

		m_client.setQmName(qMgrName);
		m_client.setQConnectName(qConnectName);
		m_client.setQChannel(qChannel);
		m_client.setExpire(topconn_string::to_long<string>(qExpire)*1000);
		m_client.setWaitInterval(topconn_string::to_long<string>(qWaitInterval) * 1000);
		m_client.setLoacalAddress(qLoacalAddress);
		m_client.setUsereId(qUserid);
		m_client.setPassword(qPassword);
	
		if (m_client.connect() != 0)
		{
			sprintf(sError, "连接队列管理器[%s]失败,reasonCode=%ld", qMgrName.c_str(),m_client.reasonCode());
			TLOG_ERROR(sError);
			throw ConnException(sError);
		}
	}
};