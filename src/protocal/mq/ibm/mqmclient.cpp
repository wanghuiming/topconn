#include "mqmclient.hpp"

namespace topconn
{
	MqmClient::MqmClient()
		: m_status(false)
		, m_waitInterval(0)
		, m_expiry(MQEI_UNLIMITED)
		, m_charset(0)
		, m_channelName("SYSTEM.DEF.SVRCONN")
		, m_reasonCode(0)
		, m_completionCode(0)
	{
	}

	MqmClient::MqmClient(const string& qmName)
		: m_status(false)
		, m_qmName(qmName)
		, m_waitInterval(0)
		, m_expiry(MQEI_UNLIMITED)
		, m_charset(0)
		, m_channelName("SYSTEM.DEF.SVRCONN")
		, m_reasonCode(0)
		, m_completionCode(0)
	{
	}

	long  MqmClient::reasonCode()
	{
		return m_reasonCode;
	}

	long  MqmClient::completionCode()
	{
		return m_completionCode;
	}

	void MqmClient::setQmName(const string& qmName)
	{
		m_qmName = qmName;
	}

	void MqmClient::setQChannel(const string& qChannel)
	{
		m_channelName = qChannel;
	}

	void MqmClient::setQConnectName(const string& qIp, const string& qPort)
	{
		m_connectName.append(qIp).append("(").append(qPort).append(")");
	}

	void MqmClient::setQConnectName(const string& qConnectName)
	{
		m_connectName = qConnectName;
	}

	void MqmClient::setLoacalAddress(const string& localAddress)
	{
		m_loacalAddress = localAddress;
	}

	void MqmClient::setExpire(long expire)
	{
		m_expiry = expire;
	}

	void MqmClient::setWaitInterval(long waitInterval)
	{
		m_waitInterval = waitInterval;
	}

	void MqmClient::setUsereId(const string& userId)
	{
		m_userId = userId;
	}
	void MqmClient::setPassword(const string& password)
	{
		m_passwd = password;
	}

	void MqmClient::setQname(const string& qName)
	{
		m_qName = qName;
	}

	int MqmClient::init()
	{
#if 0
		char sMQServer[128] = { 0 };
		sprintf(sMQServer, "%s/TCP/%s", m_channel.c_str(), m_connectName.c_str());
		setenv("MQSERVER", sMQServer, 1)
#endif //   0
		return 0;
	}

	int MqmClient::connect()
	{
		int iReturnCode = 0;

		if (m_status)
		{
			return 0;
		}

		//设置通道
		m_mqmChannel.setHeartBeatInterval(1);
		m_mqmChannel.setChannelName(m_channelName.c_str());
		m_mqmChannel.setTransportType(MQXPT_TCP);
		m_mqmChannel.setConnectionName(m_connectName.c_str());
		if(!m_loacalAddress.empty())
			m_mqmChannel.setLocalAddress(m_loacalAddress.c_str());
		if(!m_userId.empty())
			m_mqmChannel.setUserId(m_userId.c_str());
		if(!m_passwd.empty())
			m_mqmChannel.setPassword(m_passwd.c_str());

		//设置队列管理器
		m_mqmManager.setChannelReference(&m_mqmChannel);
		m_mqmManager.setName(m_qmName.c_str());
		if(m_charset > 0)
			m_mqmManager.characterSet(m_charset);

		//连接队列管理器
		if (!m_mqmManager.connect())
		{
			iReturnCode = m_mqmManager.reasonCode();
			printf("ImqQueueManager::connect ended with reason code %ld\n",
				(long)iReturnCode);
			return -1;
		}
		m_status = true;
		return 0;
	}

	int MqmClient::connect(const string& QMName,const string& QConnectName,const string& QChannelName)
	{
		setQmName(QMName);
		setQChannel(QChannelName);
		setQConnectName(QConnectName);
		return connect();
	}

	int MqmClient::currentDepth(const string& qName)
	{
		ImqQueue queue;
		queue.setName(qName.c_str());
		queue.setOpenOptions(MQOO_OUTPUT|MQOO_INPUT_SHARED|MQOO_INQUIRE);
		queue.setConnectionReference(m_mqmManager);
		queue.open();
		if (queue.reasonCode()) {
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)queue.reasonCode());
			return -1;
		}
		return queue.currentDepth();
	}

	int MqmClient::openQueue()
	{
		m_mqmQueue.setName(m_qName.c_str());
		m_mqmQueue.setOpenOptions(MQOO_OUTPUT | MQOO_INPUT_SHARED | MQOO_INQUIRE);
		m_mqmQueue.setConnectionReference(m_mqmManager);
		m_mqmQueue.open();
		if (m_mqmQueue.reasonCode()) {
			setReasonCode(m_mqmQueue);
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)m_mqmQueue.reasonCode());
			return -1;
		}
		return 0;
	}

	int MqmClient::put(const string& message)
	{
		ImqMessage qMessage;
		qMessage.useFullBuffer(message.c_str(), message.size());
		qMessage.setFormat(MQFMT_STRING);
		qMessage.setExpiry(m_expiry);

		if (!m_mqmQueue.put(qMessage))
		{
			printf("ImqQueue::put ended with reason code %ld\n",
				(long)m_mqmQueue.reasonCode());
			setReasonCode(m_mqmQueue);
			return -1;
		}
		return 0;
	}

	int MqmClient::put(const string& qName, const string& message)
	{
		return put(qName,message, string(), string());
	}

	int MqmClient::put(const string& qName, const string& message, const string& messageId)
	{
		return put(qName,message, messageId, string());
	}

	int MqmClient::put(const string& qName, const string& message, const string& messageId, const string& correlId)
	{
		ImqMessage qMessage;
		qMessage.useFullBuffer(message.c_str(), message.size());
		qMessage.setFormat(MQFMT_STRING);
		qMessage.setMessageId((const unsigned char*)messageId.c_str());
		qMessage.setCorrelationId((const unsigned char*)correlId.c_str());
		qMessage.setExpiry(m_expiry);
		return put(qName,qMessage);
	}

	int MqmClient::put(const string& qName, ImqMessage& qMessage)
	{
		ImqQueue queue;
		queue.setName(qName.c_str());
		queue.setOpenOptions(MQOO_OUTPUT | MQOO_INPUT_SHARED | MQOO_INQUIRE);
		queue.setConnectionReference(m_mqmManager);

		queue.open();
		if (queue.reasonCode()) {
			setReasonCode(queue);
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)queue.reasonCode());
			return -1;
		}

		if (!queue.put(qMessage))
		{
			printf("ImqQueue::put ended with reason code %d\n",
				(int)queue.reasonCode());
			setReasonCode(queue);
			return -1;
		}
		return 0;
	}

	int MqmClient::get(string& message)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		gmo.setMatchOptions(MQMO_NONE);

		if (!m_mqmQueue.get(qMessage, gmo))
		{
			printf("ImqQueue::get ended with reason code %ld\n",
				(long)m_mqmQueue.reasonCode());
			setReasonCode(m_mqmQueue);
			return -1;
		}
		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		return 0;
	}

	int MqmClient::get(const string& qName, string& message)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		gmo.setMatchOptions(MQMO_NONE);

		if (get(qName, qMessage, gmo) != 0)
		{
			return -1;
		}
		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		return 0;
	}

	int MqmClient::get(const string& qName, const string& messageId, string& message)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		gmo.setMatchOptions(MQMO_MATCH_MSG_ID);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		qMessage.setMessageId((const unsigned char*)messageId.c_str());

		if (get(qName, qMessage, gmo) != 0)
		{
			return -1;
		}
		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		return 0;
	}

	int MqmClient::get(const string& qName, const string& correlId, string& message, string& messageId)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		gmo.setMatchOptions(MQMO_MATCH_CORREL_ID);
		qMessage.setCorrelationId((const unsigned char*)messageId.c_str());

		if (get(qName, qMessage, gmo) != 0)
		{
			return -1;
		}

		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		ImqBinary coreelationid = qMessage.correlationId();
		messageId = string((char*)coreelationid.dataPointer(), coreelationid.dataLength());
		return 0;
	}

	int MqmClient::get(const string& qName, ImqMessage& qMessage, ImqGmo3& gmo)
	{
		ImqQueue queue;
		queue.setName(qName.c_str());
		queue.setOpenOptions(MQOO_OUTPUT | MQOO_INPUT_SHARED | MQOO_INQUIRE);
		queue.setConnectionReference(m_mqmManager);

		queue.open();
		if (queue.reasonCode()) {
			setReasonCode(queue);
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)queue.reasonCode());
			return -1;
		}

		if (!queue.get(qMessage, gmo))
		{
			printf("ImqQueue::get ended with reason code %d\n",
				(int)queue.reasonCode());
			setReasonCode(queue);
			return -1;
		}
		return 0;
	}

	int MqmClient::browse(const string& qName, string& message)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		gmo.setOptions(MQGMO_BROWSE_NEXT | gmo.options() );
		gmo.setMatchOptions(MQMO_NONE);

		ImqQueue queue;
		queue.setName(qName.c_str());
		queue.setOpenOptions(MQOO_BROWSE);
		queue.setConnectionReference(m_mqmManager);

		queue.open();
		if (queue.reasonCode()) {
			setReasonCode(queue);
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)queue.reasonCode());
			return -1;
		}

		if (!queue.get(qMessage, gmo))
		{
			printf("ImqQueue::get ended with reason code %d\n",
				(int)queue.reasonCode());
			setReasonCode(queue);
			return -1;
		}
		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		return 0;
	}

	int MqmClient::browse(const string& qName, const string& messageId, string& message)
	{
		ImqMessage qMessage;
		ImqGmo3 gmo;
		gmo.setWaitInterval(m_waitInterval);
		if (m_waitInterval > 0)
			gmo.setOptions(MQGMO_WAIT);
		gmo.setOptions(MQGMO_BROWSE_NEXT | gmo.options());
		gmo.setMatchOptions(MQMO_MATCH_MSG_ID);
		qMessage.setMessageId((const unsigned char*)messageId.c_str());

		if (browse(qName, qMessage,gmo) != 0)
		{
			return -1;
		}
		message = string(qMessage.bufferPointer(), qMessage.dataLength());
		return 0;
	}

	int MqmClient::browse(const string& qName, ImqMessage& qMessage, ImqGmo3& gmo)
	{
		ImqQueue queue;
		queue.setName(qName.c_str());
		queue.setOpenOptions(MQOO_BROWSE);
		queue.setConnectionReference(m_mqmManager);

		queue.open();
		if (queue.reasonCode()) {
			setReasonCode(queue);
			printf("ImqQueue::open ended with reason code %ld\n",
				(long)queue.reasonCode());
			return -1;
		}

		if (!queue.get(qMessage, gmo))
		{
			printf("ImqQueue::get ended with reason code %d\n",
				(int)queue.reasonCode());
			setReasonCode(queue);
			return -1;
		}
		return 0;
	}

	void MqmClient::setReasonCode(ImqQueueManager& qmgr)
	{
		m_reasonCode = qmgr.reasonCode();
		m_completionCode = qmgr.completionCode();
	}

	void MqmClient::setReasonCode(ImqQueue& queue)
	{
		m_reasonCode = queue.reasonCode();
		m_completionCode = queue.completionCode();
	}
};