#pragma once

#include "sockutil.h"

namespace topconn
{
	class TcpClient
    {
	protected:
        int m_sockFd;
        string   m_addr;
        uint16_t m_port;
        uint16_t m_maxTimeout;
        uint16_t m_maxConnTimeout;
        uint16_t m_length;
        bool m_isBlock;
        bool m_isIpv6;
        bool m_isReconn;
        bool m_isKeepalive;
        uint64_t m_lastActiveTime;

	public:
        TcpClient();
        TcpClient(const string& addr, uint16_t port);
        TcpClient(const string& addr, uint16_t port,uint16_t timeout);
        virtual ~TcpClient();

        void setAddr(const string& addr, uint16_t port);
        void setMaxTimeout( uint16_t maxTimeout);
        void setMaxConnTimeout( uint16_t maxConnTimeout);
        void setLength(uint16_t length);
        void setBlock(bool on);
        void setIpv6(bool on);
        void setReconnect(bool on);
        void setKeepalive(bool on);

        const string& getAddr() const;
        uint16_t getPort() const;
        uint16_t getMaxTimeout() const;
        uint16_t getMaxConnTimeout() const;
        uint16_t getLength() const;
        bool isBlock() const;
        bool isIpv6() const;
        bool isReconnect() const;
        bool isKeepalive() const;

        int connect();
        int disConnect();
        bool isConnected();

        int sendto(const string& sndbuf);
        int recvfrom(string& rcvbuf);
    private:
        void updateActiveTime();
	};
}