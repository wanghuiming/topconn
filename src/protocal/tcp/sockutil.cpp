#include "sockutil.h"

#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <assert.h>
#include <netdb.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <sys/un.h>

#ifdef __linux__
#include <linux/version.h>
#endif

using namespace std;

namespace topconn
{
	int SockUtil::create(bool isBlock,bool isIPV6)
	{
		int option = SOCK_STREAM | SOCK_CLOEXEC;
		if(!isBlock)
		{
			option |= SOCK_NONBLOCK;
		}
		int fd = socket(isIPV6 ? AF_INET6 : AF_INET, option, IPPROTO_TCP);
		if(fd < 0)
		{
			return fd;
		}
		return fd;
	}	

    int SockUtil::bindAndListen(int port, bool isBlock,bool isIpv6)
    {
        int err = 0;
        int fd = SockUtil::create(isBlock,isIpv6);
        if(fd < 0)
        {
            err = errno;
            return fd;
        }
        SockUtil::setReuseable(fd, true);
		SockUtil::setReuseport(fd, true);	
        do
        {
			if(isIpv6)
			{
				struct sockaddr_in6 sockAddr;
				sockAddr.sin6_family = AF_INET6;
				sockAddr.sin6_addr = in6addr_any;
				sockAddr.sin6_port = htons(port);
				if(bind(fd, (struct sockaddr*)&sockAddr, sizeof(sockAddr)) < 0)
				{
					err = errno;
					break;    
				}
				if(listen(fd, SOMAXCONN) < 0)
				{
					err = errno;
					break;     
				}				
			}
			else
			{
				struct sockaddr_in sockAddr;
				sockAddr.sin_family = AF_INET;
				sockAddr.sin_addr.s_addr = htonl(INADDR_ANY); 
				sockAddr.sin_port = htons(port);
				if(bind(fd, (struct sockaddr*)&sockAddr, sizeof(sockAddr)) < 0)
				{
					err = errno;
					break;    
				}
				if(listen(fd, SOMAXCONN) < 0)
				{
					err = errno;
					break;     
				}				
			}
            return fd;
        }while(0);
        close(fd);
        return -1;
    }

	int SockUtil::connect(int sockFd, const string ipaddr, const int port,bool isIPV6)
	{
		int ret = 0;
		if(isIPV6)
		{
			struct sockaddr_in6 sockAddr;
			bzero(&sockAddr, sizeof(sockAddr));
			sockAddr.sin6_family = AF_INET6;
			sockAddr.sin6_port = htons(port);		

			if(inet_pton(AF_INET6, ipaddr.c_str(), &sockAddr.sin6_addr) <= 0)
			{
				char addr[128]={0};
				uint32_t ret = SockUtil::getHostByName(ipaddr,addr,sizeof(addr));
				if(ret == uint32_t(-1) )
				{
					return -2;
				}
				if( inet_pton(AF_INET6, addr, &sockAddr.sin6_addr) <= 0)
				{
					return -2;
				}
			}
			ret = ::connect(sockFd, (struct sockaddr*)&sockAddr, sizeof(sockAddr));
		}
		else
		{
			struct sockaddr_in sockAddr;
			bzero(&sockAddr, sizeof(sockAddr));
			sockAddr.sin_family = AF_INET;
			sockAddr.sin_port = htons(port);

			if(inet_pton(AF_INET, ipaddr.c_str(), &sockAddr.sin_addr) <= 0)
			{
				uint32_t ret = SockUtil::getHostByName(ipaddr);
				if(ret == uint32_t(-1) )
				{
					return -2;
				}
				sockAddr.sin_addr.s_addr = ret;
			}
			ret = ::connect(sockFd, (struct sockaddr*)&sockAddr, sizeof(sockAddr));
		}
	  if(ret < 0)
	  {
	      int err = errno;
	      return err;
	  }
	  else
	  {
	      SockUtil::setNoDelay(sockFd, true);
	      return ret;
	  }
	}

    int SockUtil::setNoDelay(int sockFd, bool on)
    {
        int opt = on ? 1 : 0;
        return setsockopt(sockFd, IPPROTO_TCP, 
                        TCP_NODELAY, &opt, 
                        static_cast<socklen_t>(sizeof(opt)));    
    }
    
    int SockUtil::setReuseable(int sockFd, bool on)
    {
        int opt = on ? 1 : 0;
        return setsockopt(sockFd, SOL_SOCKET, 
                        SO_REUSEADDR, &opt,
                        static_cast<socklen_t>(sizeof(opt)));    
    }

	int SockUtil::setReuseport(int sockFd, bool on)
	{
		 int opt = on ? 1 : 0;
#ifdef __linux__
	#if LINUX_VERSION_CODE > KERNEL_VERSION(3,9,0)
         return setsockopt(sockFd, SOL_SOCKET, 
            SO_REUSEPORT, &opt,
            static_cast<socklen_t>(sizeof(opt))); 	
	#endif	
#endif		 
		return 0;	 
	}

    int SockUtil::setKeepAlive(int sockFd, bool on)
    {
        int opt = on ? 1 : 0;
        return setsockopt(sockFd, SOL_SOCKET, 
                        SO_KEEPALIVE, &opt,
                        static_cast<socklen_t>(sizeof(opt)));    
    }

	int  SockUtil::setSndTimeout(int sockFd, int timeout)
	{
		struct timeval t={0,0};
		t.tv_usec = timeout;
        t.tv_sec = 0;
	    if( ::setsockopt(sockFd,SOL_SOCKET,SO_SNDTIMEO,&t,sizeof(t)) == -1 )
        {
            return -1;
        }
		return 0;
	}

	int  SockUtil::setRcvTimeout(int sockFd, int timeout)
	{
		struct timeval t={0,0};
		t.tv_usec = timeout;
        t.tv_sec = 0;
	    if( ::setsockopt(sockFd,SOL_SOCKET,SO_RCVTIMEO,&t,sizeof(t)) == -1 )
        {
            return -1;
        }
		return 0;
	}

    int SockUtil::getLocalAddr(int sockFd, string& addr, bool isIPV6)
    {
		char buf[128] = { '\0' };
		if(isIPV6)
		{
			struct sockaddr_in6 sockAddr;
			socklen_t sockLen = sizeof(sockAddr);
			if(getsockname(sockFd, (struct sockaddr*)&sockAddr, &sockLen) != 0)
			{
				int err = errno;
				return err;
			}
			inet_ntop(AF_INET6, &sockAddr.sin6_addr, buf, static_cast<socklen_t> (sizeof(buf)));
		}
		else 
		{
			struct sockaddr_in sockAddr;
			socklen_t sockLen = sizeof(sockAddr);
			if(getsockname(sockFd, (struct sockaddr*)&sockAddr, &sockLen) != 0)
			{
				int err = errno;
				return err;
			}
			inet_ntop(AF_INET, &sockAddr.sin_addr, buf, static_cast<socklen_t> (sizeof(buf)));
		}
		addr = string(buf);
        return 0;
    }

    int SockUtil::getRemoteAddr(int sockFd, string& addr, bool isIPV6)
    {
		char buf[128] = { '\0' };
		if(isIPV6)
		{
			struct sockaddr_in6 sockAddr;
			socklen_t sockLen = sizeof(sockAddr);
			if(getpeername(sockFd, (struct sockaddr*)&sockAddr, &sockLen) != 0)
			{
				int err = errno;
				return err;
			}
			inet_ntop(AF_INET6, &sockAddr.sin6_addr, buf, static_cast<socklen_t> (sizeof(buf)));
		}
		else 
		{
			struct sockaddr_in sockAddr;
			socklen_t sockLen = sizeof(sockAddr);
			if(getpeername(sockFd, (struct sockaddr*)&sockAddr, &sockLen) != 0)
			{
				int err = errno;
				return err;
			}
			inet_ntop(AF_INET, &sockAddr.sin_addr, buf, static_cast<socklen_t> (sizeof(buf)));
		}
		addr = string(buf);
        return 0;
    }

    int SockUtil::getSockError(int sockFd)
    {
        int opt;
        socklen_t optLen = static_cast<socklen_t>(sizeof(opt));
        if(getsockopt(sockFd, SOL_SOCKET, SO_ERROR, &opt, &optLen) < 0)
        {
            int err = errno;
            return err;    
        }   
        else
        {
            return opt;    
        }
    }

    uint32_t SockUtil::getHostByName(const string& host)
    {
        struct addrinfo hint;
        struct addrinfo *answer;

        memset(&hint, 0, sizeof(hint));
        hint.ai_family = AF_INET;
        hint.ai_socktype = SOCK_STREAM;

        int ret = getaddrinfo(host.c_str(), NULL, &hint, &answer);
        if(ret != 0)
        {
            return uint32_t(-1);
        }

        //we only use first find addr
        for(struct addrinfo* cur = answer; cur != NULL; cur = cur->ai_next)
        {
			uint32_t addr = ((struct sockaddr_in*)(cur->ai_addr))->sin_addr.s_addr;
			freeaddrinfo(answer);
            return addr;
        }
		freeaddrinfo(answer);
        return uint32_t(-1);
    }

	uint32_t SockUtil::getHostByName(const std::string& host,char* ipv6addr,size_t ipv6addr_size)
	{
        struct addrinfo hint;
        struct addrinfo *answer;

        memset(&hint, 0, sizeof(hint));
        hint.ai_family = AF_INET6;
        hint.ai_socktype = SOCK_STREAM;

		if(!ipv6addr) return uint32_t(-1);

        int ret = getaddrinfo(host.c_str(), NULL, &hint, &answer);
        if(ret != 0)
        {
            return uint32_t(-1);
        }

        //we only use first find addr
        for(struct addrinfo* cur = answer; cur != NULL; cur = cur->ai_next)
        {
			struct sockaddr_in6 *sockaddr_ipv6 = reinterpret_cast<struct sockaddr_in6 *>( cur->ai_addr); 
			if( inet_ntop(AF_INET6, &sockaddr_ipv6->sin6_addr, ipv6addr,ipv6addr_size) == 0)
			{
				continue;
			}
			freeaddrinfo(answer);
            return 0;
        }
		freeaddrinfo(answer);
        return uint32_t(-1);
	}

    int SockUtil::bindDevice(int sockFd, const string& device,bool isIPV6)
    {
    	if(isIPV6)
    	{
			char *line = NULL;
			size_t len = 0;
			ssize_t read;
			char ipv6_addr1[60]={0};
			char ipv6_addr2[60]={0}; 
			struct sockaddr_in6 sin; 
			bzero(&sin, sizeof(sin));	  

			FILE * fp = fopen("/proc/net/if_inet6", "r");
			if (fp == NULL) 
			{
				return -1;
			}

			while ((read = getline(&line, &len, fp)) != -1)  
			{
				if( strstr(line,device.c_str()) == NULL) 
				{
					free(line); line = NULL;
					continue;
				}
				strncpy(ipv6_addr1,line,32);
				int addr1_len = strlen(ipv6_addr1);
				for(int i=0; i <  addr1_len;)
				{
					if(i != 0) 
					{
						ipv6_addr2[strlen(ipv6_addr2)] = ':';
					}
					strncat(ipv6_addr2,ipv6_addr1+i,4);
					i=i+4;
				}
				break;
			} 
			free(line);

			if(strlen(ipv6_addr2) == 0) return -1;

			sin.sin6_family = AF_INET6;
			sin.sin6_port = 0;
			if(inet_pton(AF_INET6, ipv6_addr2, &sin.sin6_addr) <= 0)
			{
				return -1;  
			} 
			if(bind(sockFd, (struct sockaddr*)&sin, sizeof(sin)) < 0)
			{
				return -1;
			} 			 	
    	}
		else
		{
			struct ifreq ifr;
			memset(&ifr, 0, sizeof(ifr));

			snprintf(ifr.ifr_name, sizeof(ifr.ifr_name), "%s",device.c_str());
			struct sockaddr_in *sin=(struct sockaddr_in*)&ifr.ifr_addr;

			//get ipv4 addr by device
			if(ioctl(sockFd, SIOCGIFADDR, &ifr) < 0)
			{
				return -1;
			}

			sin->sin_family = AF_INET;
			sin->sin_port = 0;
			if(bind(sockFd, (struct sockaddr*)sin, sizeof(*sin)) < 0)
			{
				return -1;
			} 
		}
        return 0;
    }

    int SockUtil::accept(int sockFd)
    {
    	while(true)
    	{
            int acceptSock = ::accept4(sockFd, NULL, NULL, SOCK_NONBLOCK|SOCK_CLOEXEC);
            if(acceptSock < 0)
            {
                int err = errno;
                if(err == EMFILE || err == ENFILE)
                {
                    acceptSock = ::accept(sockFd, NULL, NULL);
                    close(acceptSock);
                    return -1;
                }
                if(errno == EAGAIN || errno == EWOULDBLOCK )
                {
                	break;
                }
                continue;
            }
            else
            {
            	SockUtil::setNoDelay(acceptSock, true);
                return acceptSock;
            }
    	}
    	return -1;
    }

	int SockUtil::accept1(int sockFd)
	{
		int acceptSock = ::accept4(sockFd, NULL, NULL, SOCK_CLOEXEC);
		if(acceptSock < 0)
		{
			int err = errno;
			if(err == EMFILE || err == ENFILE)
			{
				acceptSock = ::accept(sockFd, NULL, NULL);
				close(acceptSock);
				return -1;
			}
			if(errno == EAGAIN || errno == EWOULDBLOCK )
			{
				return 0;
			}
		}
		else
		{
			SockUtil::setNoDelay(acceptSock, true);
			return acceptSock;
		}
		return -1;
	}	

	/**
	 * @brief 
	 * 在非阻塞模式下，send函数的过程是将应用程序请求发送的数据拷贝内核发送缓存中,区别在于非堵塞模式下，
	 * send函数不需要等到待发送数据完全被拷贝到内核发送区中才返回。
	 * 如果内核缓存区可用空间不够容纳所有待发送数据，则尽能力的拷贝，返回成功拷贝的大小；
	 * 如果缓存区可用空间为0,则返回-1,同时设置errno为EAGAIN.
	 * 
	 * 在阻塞模式下，send函数的过程是将应用程序请求发送的数据拷贝到内核发送缓存中，
	 * 待发送数据完全被拷贝到内核发送缓存区中才返回，如果内核发送缓存区一直没有空间能容纳待发送的数据,则一直阻塞;
	 * 
	 * @param sockFd 
	 * @param rcvBuf 
	 * @param buff_Size 
	 * @param iRcvLen 
	 * @return int 
	 */
    int  SockUtil::send(int sockFd,const void* sndBuf, int   iLen )
    {
		int nSndLen = 0;
		time_t t1,t2;  
		time(&t1); 

		while(nSndLen < iLen)
		{
			int nLen = ::send( sockFd,(char*)sndBuf+nSndLen,iLen,0 );
			if(nLen < 0)
			{
				if(errno == EAGAIN || errno == EWOULDBLOCK)
				{
					time(&t2);  
					if(difftime(t2,t1) >= 4)	
					{
						return nSndLen > 0 ? nSndLen : -1;
					}			
					SockUtil::wait( 0, 100*1000 );
					continue;
				}
				return -1;
			}
			nSndLen  += nLen;
		}
		return nSndLen;
    }

	int  SockUtil::send(int sockFd,const string& sndBuf)
    {
		return SockUtil::send(sockFd,sndBuf.c_str(), sndBuf.length() );
	}

	/**
	 * @brief 
	 * 在非阻塞模式下,如果内核缓冲区没有数据可读，recv 将立即返回 -1，设置 errno 为 EAGAIN 或 ewoudblock。
	 * 因此，在循环中调用recv，直到得到所需的量，同时检查返回码是0(另一端断开)还是 -1(一些错误)。
	 * 在阻塞模式下,如果内核缓冲区没有数据可读， recv ()会阻塞，直到有一些数据存在可以读取为止。
	 * 然后，它将返回读取到的数据(可能少于请求的数量len) ，返回的数最大为len。
	 * 
	 * @param sockFd 
	 * @param rcvBuf 
	 * @param buff_Size 
	 * @param iRcvLen 
	 * @return int 
	 */
    int  SockUtil::recv(int sockFd,void* rcvBuf,int  buff_Size,int  iRcvLen,int cycle_option)
    {
		if(iRcvLen > buff_Size)
		{
			return -1;
		}

		int nRcvLen=0;
		time_t t1,t2;  
		time(&t1); 		
		do
		{
			int nLen = ::recv(sockFd,(char*)rcvBuf+nRcvLen,iRcvLen-nRcvLen,0);
			if(nLen < 0) 
			{
				if(errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR )
				{
					time(&t2);  
					if( difftime(t2,t1) >= 4)	
					{
						return nRcvLen > 0 ? nRcvLen :-1;
					}				
					SockUtil::wait( 0, 100*1000 );
					continue;
				}
				return -1;
			} 
			else if(nLen == 0) 
			{
				return 0;
			}
			nRcvLen += nLen;
		}while(nRcvLen < iRcvLen && cycle_option);
		return nRcvLen;
    }

	int  SockUtil::recv(int sockFd,string& rcvBuf,int  iRcvLen,int cycle_option)
    {
		int iRet = 0;
		char sRcvBuf[8192]={0};
		if(iRcvLen < sizeof(sRcvBuf))
		{
			iRet = SockUtil::recv(sockFd,sRcvBuf,sizeof(sRcvBuf),iRcvLen,cycle_option);
			rcvBuf = sRcvBuf;
		}
		else
		{
			char* pRcvBuf = (char*)calloc(iRcvLen+1,sizeof(char));
			if(!pRcvBuf)
			{
				return -1;
			}
			iRet = SockUtil::recv(sockFd,pRcvBuf,iRcvLen+1,iRcvLen,cycle_option);
			rcvBuf = pRcvBuf;
			free(pRcvBuf);
		}
		return iRet;
	}	

	int SockUtil::sendLongData(int sockFd, char* sndBuf, int sndLen)
	{
		#define MAX_LONG_LENGTH  1024
		char* p1 = sndBuf;
		int len  = sndLen;
		while( 1 )
		{
			if( len >= MAX_LONG_LENGTH ) 
			{
				if( SockUtil::send( sockFd,p1,MAX_LONG_LENGTH ) != 0 )
					return	-1;
				p1 += MAX_LONG_LENGTH;
				len -= MAX_LONG_LENGTH;
			} 
			else if( len > 0 ) 
			{
				if( SockUtil::send( sockFd,p1,len ) != 0 )
					return	-1;
				break;
			} 
			else 
			{
				break;
			}
		}
		#undef MAX_LONG_LENGTH
		return	sndLen - len;
	}	

	void SockUtil::wait(int tvSev, int tvUSev)
	{
		fd_set 	Set;
		struct timeval timeout;
		timeout.tv_usec = tvUSev;
		timeout.tv_sec = tvSev;
		int ready = ::select( 0,NULL,NULL,NULL,&timeout );
		return;
	}

	int SockUtil::isReadAble(int sockFd,int timeout)
	{
		fd_set readfds;
		fd_set writefds;
		struct timeval t{0,0};
	
		FD_ZERO( &readfds );
		FD_SET( sockFd, &readfds );

		//default timeout 5 seconds
		t.tv_sec = timeout < 0 ? 5 : timeout; 
		t.tv_usec = 0;
		return select( sockFd + 1, &readfds,NULL, NULL, &t );
	}	

	int SockUtil::isWriteAble(int sockFd,int timeout)
	{
		int iRet = 0;
		fd_set readfds;
		fd_set writefds;
		struct timeval t{0,0};
	
		FD_ZERO( &writefds );
		FD_SET( sockFd, &writefds );

		//default timeout 5 seconds
		t.tv_sec = timeout < 0 ? 5 : timeout; 
		t.tv_usec = 0;
		iRet = select( sockFd + 1, NULL,&writefds, NULL, &t );
		if(iRet <= 0)
		{
			return iRet;
		}

		if ( ! FD_ISSET( sockFd, &writefds  ) )
		{
			return -1;
		}
	
		int error = 0;
		socklen_t length = sizeof( error );
		if( getsockopt( sockFd, SOL_SOCKET, SO_ERROR, &error, &length ) < 0 )
		{
			return -1;
		}
		if( error != 0 )
		{
			return -1;
		}	
		return 	iRet;
	}	

	int SockUtil::getIpByHostname(const char* hostname,char* ip)
	{
		struct hostent *he;
		if ((he = ::gethostbyname(hostname)) == NULL) 
		{
			printf("hostname error for %s:%s\n", hostname,	hstrerror(h_errno));
			return -1;
		}	
		char destIP[128];
		char **phe = NULL;
		for( phe=he->h_addr_list ; NULL != *phe ; ++phe)
		{
			inet_ntop(he->h_addrtype,*phe,destIP,sizeof(destIP));
			break;
		}	
		strcpy(ip,destIP);
		return 0;
	}	

	int SockUtil::getPortByService(const char* service)
	{
		int port = -1;
		struct servent *sptr;
		if((sptr=::getservbyname(service,"tcp")) == NULL)
		{
			printf("hostname error for %s:%s\n", service,	hstrerror(h_errno));
			return -1;
		}		
		port = ntohs(sptr->s_port);
		return port;
	}

	int SockUtil::getHostnameByIp(const char* saddr,char* hostname)
	{
	    struct hostent *host;
		char paddr[32];
		inet_pton(AF_INET, saddr, paddr);
		host = gethostbyaddr(paddr, strlen(paddr), AF_INET);
		if(host == NULL)
		{
			printf("saddr error for %s:%s\n", saddr,	hstrerror(h_errno));
			return -1;
		}
		strcpy(hostname,host->h_name);	
		char **phe = NULL;
		for( phe=host->h_aliases ; NULL != *phe ; ++phe)
		{
			strcpy(hostname,*phe);
		}
		return 0;
	}

	bool SockUtil::isClosed(int sockFd)
	{
		int iRet = 0;
		iRet = SockUtil::isReadAble(sockFd,0);
		if( iRet > 0 )
		{
			char buff[2]={0}; 
			int recvBytes = recv(sockFd, buff, 1, MSG_PEEK); 
			if( recvBytes > 0) 
				return false; 
			else if(recvBytes == 0)
				return true;	
			//No receive data 
			if( (recvBytes == -1) && 
				(errno == EWOULDBLOCK || errno == EAGAIN) ) 
				return false; 
			return true;	
		}
		else if(iRet == 0)
			return false; 
		return true;
	}

	int SockUtil::createUDS(bool isBlock)
	{
		int option = SOCK_STREAM  | SOCK_CLOEXEC;
		if(!isBlock)
		{
			option |= SOCK_NONBLOCK;
		}
		return ::socket(AF_UNIX, option, 0);
	}	

	int SockUtil::bindAndListenUDS(const string& un_path)
	{
		int err;
		int fd = createUDS();
		if(fd < 0)
		{
			err = errno;
			return -1;
		}

		if (remove(un_path.c_str()) == -1 && errno != ENOENT)
		{
			err = errno;
			return -2;
		}

		struct sockaddr_un un;
		un.sun_family = AF_UNIX;
		unlink(un_path.c_str());
		strcpy(un.sun_path,un_path.c_str());
		if(bind(fd,(struct sockaddr *)&un,sizeof(un)) <0 )
		{
			err = errno;
			return -3;
		}
	#ifndef SOMAXCONN
		#define SOMAXCONN 1024
	#endif
		if(::listen(fd, SOMAXCONN) < 0)
		{
			err = errno;
			return -4;     
		}	
		return fd;
	}	

	int SockUtil::connectUDS(const string& un_path,bool isBlock)
	{
		int sockFd = SockUtil::createUDS(isBlock);		
		if(sockFd < 0)
		{
			return -1;
		}	
		struct sockaddr_un un;
		un.sun_family = AF_UNIX;
		strcpy(un.sun_path,un_path.c_str());	
		if(::connect(sockFd,(struct sockaddr *)&un,sizeof(un)) < 0)
		{
			return -2;
		}	
		return sockFd;
	}

	int SockUtil::connectUDS(int sockFd,const string& un_path)
	{
		struct sockaddr_un un;
		un.sun_family = AF_UNIX;
		strcpy(un.sun_path,un_path.c_str());	
		if(::connect(sockFd,(struct sockaddr *)&un,sizeof(un)) < 0)
		{
			return -1;
		}	
		return 0;
	}	

	bool SockUtil::checkTCPPortStatus(int port,const char* ipaddr )
	{
		int sockFd = SockUtil::create(true,false);
		if(sockFd > 0)
		{
			if( SockUtil::connect(sockFd,ipaddr,port) == 0)
			{
				close(sockFd);
				return true;
			}
			close(sockFd); 
		}	
		return false;
	}	
}
