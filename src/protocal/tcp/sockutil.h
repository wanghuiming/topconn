#pragma once

#include <string>
#include <stdint.h>

#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
#include <netdb.h>
#include <arpa/inet.h>

using namespace std;

namespace topconn
{
    class SockUtil
    {
    public:
		/**
		 * @brief 创建套接字(SOCK_STREAM)
		 * 
		 * @param isBlock 是否阻塞标志 
		 * @param isIPV6  是否为IPV6
		 * @return int 
		 *   > 0 已生成的套接字 
		 *   < 0 失败
		 */
		static int  create(bool isBlock = false,bool isIPV6=false);
		static int  connect(int sockFd, const string ipaddr, const int port,bool isIPV6 = false );
		static int  bindAndListen(int port, bool isBlock = true,bool isIpv6 = false);
		static int  bindDevice(int sockFd, const string& device, bool isIPV6);
		static int  accept(int sockFd);

		/**
		 * @brief 设置socket选项
		 * 
		 * @param sockFd 
		 * @param on 
		 * @return int 
		 */
		static int  setNoDelay(int sockFd,   bool on);
		static int  setReuseable(int sockFd, bool on);
		static int  setReuseport(int sockFd, bool on);
		static int  setKeepAlive(int sockFd, bool on);
		static int  setSndTimeout(int sockFd, int timeout);
		static int  setRcvTimeout(int sockFd, int timeout);

		/**
		 * @brief 获取本地地址
		 * 
		 * @param sockFd 
		 * @param addr 
		 * @param isIPV6 
		 * @return int 
		 */
		static int  getLocalAddr(int sockFd,  string& addr,bool isIPV6 = false);
		/**
		 * @brief 获取对端地址
		 * 
		 * @param sockFd 
		 * @param addr 
		 * @param isIPV6 
		 * @return int 
		 */
		static int  getRemoteAddr(int sockFd, string& addr,bool isIPV6 = false);
		/**
		 * @brief 获取socket错误
		 * 
		 * @param sockFd 
		 * @return int 
		 */
		static int  getSockError(int sockFd);

		/**
		 * @brief 根据主机名获取IP地址(10进制非点分格式)
		 * 
		 * @param host 
		 * @return uint32_t 
		 */
		static uint32_t getHostByName(const std::string& host);
		static uint32_t getHostByName(const std::string& host,char* ipv6addr,size_t ipv6addr_size);

		/**
		 * @brief send数据
		 * 
		 * @param sockFd  套接字
		 * @param sndBuf  待发送数据缓冲区
		 * @param iLen    待发送数据长度
		 * @return int    
		 * 	    > 0 实际发送的数据长度
		 *      < 0 发送错误
		 */
		static int  send(int sockFd,const void* sndBuf, int  iLen);	
		static int  send(int sockFd,const string& sndBuf);

		/**
		 * @brief recv数据
		 * 
		 * @param sockFd    套接字
		 * @param rcvBuf    接收缓冲区
		 * @param buff_Size 接收缓冲区大小
		 * @param iRcvLen   待接收数据长度
		 * @param option    1-循环接收
		 * @return int      
		 * 		>0 实际接收到的数据长度
		 *      =0 对端主动关闭
		 *      <0 接收出错
		 */
		static int  recv(int sockFd,void* rcvBuf,  int  buff_Size,int iRcvLen,int cycle_option=0);
		static int  recv(int sockFd,string& rcvBuf,int  iRcvLen,int cycle_option=0);

		/**
		 * @brief 根据主机域名获取IP地址(xxx.xxx.xxx.xxx)
		 * 
		 * @param hostname 
		 * @param ip 
		 * @return int 
		 */
		static int  getIpByHostname(const char* hostname,char* ip);
		/**
		 * @brief 根据端口服务名获取端口号(/etc/services)
		 * 
		 * @param service 
		 * @return int 
		 */
		static int  getPortByService(const char* service);
		/**
		 * @brief 根据IP地址获取主机服务名(/etc/hosts)
		 * 
		 * @param saddr 
		 * @param hostname 
		 * @return int 
		 */
		static int  getHostnameByIp(const char* saddr,char* hostname);
		/**
		 * @brief 检查套接字是否已关闭
		 * 
		 * @param sockFd 
		 * @return true 
		 * @return false 
		 */
		static bool isClosed(int sockFd);
		/**
		 * @brief 检查套接字是否可读
		 * 
		 * @param sockFd 
		 * @param timeout 
		 * @return int 
		 *    > 0 未超时
		 *    = 0 超时
		 *    < 0 发生错误
		 */
		static int  isReadAble( int sockFd,int timeout);
		static int  isWriteAble(int sockFd,int timeout);
		/**
		 * @brief 检查端口状态是否开启
		 * 
		 * @param port 
		 * @param ipaddr 
		 * @return true 
		 * @return false 
		 */
		static bool checkTCPPortStatus(int port,const char* ipaddr ="127.0.0.1" );
		static void wait(int tvSev, int tvUSev);	

		/**
		 * @brief 发送长数据
		 * 
		 * @param sockFd 
		 * @param sndBuf 
		 * @param sndLen 
		 * @return int 
		 */
		static int  sendLongData(int sockFd, char* sndBuf, int sndLen);
		/*accetp套接字，新套接字为阻塞套接字*/
		static int  accept1(int sockFd); 
		/*unix domain socket*/
		static int  createUDS(bool isBlock = false);
		static int  bindAndListenUDS(const string& un_path);
		static int  connectUDS(int sockFd,const string& un_path);
		static int  connectUDS(const string& un_path,bool isBlock = true);
    };
}

