#pragma once

#include "topconn_cfg.hpp"
#include "topconn_error.hpp"
#include "topconn_connector.hpp"
#include "tcpclient.hpp"

namespace topconn
{
	class TcpConnector : public Connector
	{
	protected:
		TcpClient m_client;
	public:
		TcpConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};