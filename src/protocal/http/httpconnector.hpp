#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"
#include "httpclient.hpp"

namespace topconn
{
	class HttpConnector : public Connector
	{
	protected:
		HttpClient m_client;
	public:
		HttpConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};