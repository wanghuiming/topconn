#pragma once

#include <string>
#include <memory>
using namespace std;

namespace httplib
{
	class Client;
};

namespace topconn
{
	typedef std::shared_ptr<httplib::Client>  HttpClientPtr_t;

	class HttpClient
	{
	private:
		HttpClientPtr_t m_httpClientPtr;
	protected:
		string m_url;
		uint32_t m_maxConnTimeout;
		uint32_t m_maxRecvTimeout;
		bool m_isIpv6;
		bool m_isKeepalive;

		string m_username;
		string m_password;
	public:
		HttpClient();
		HttpClient(const string& url);
		virtual ~HttpClient();

		void setUrl(const string& url);
		void setMaxTimeout(uint16_t maxTimeout);
		void setMaxConnTimeout(uint16_t maxConnTimeout);
		void setIpv6(bool on);
		void setKeepalive(bool on);
		void setUserAndPass(const string& username,const string& password);
		const string& getUrl() const;
		uint16_t getMaxTimeout() const;
		uint16_t getMaxConnTimeout() const;
		bool isIpv6() const;
		bool isKeepalive() const;

		virtual int initClient();
		virtual int request(const string& method,const string& path,const string& body,const string& content_type,string& response);
	};
};