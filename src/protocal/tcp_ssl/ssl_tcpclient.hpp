#pragma once

#include <string>
#include <memory>
using namespace std;

#include "protocal/tcp/tcpclient.hpp"
#include "ssl_util.hpp"

namespace topconn
{
	class SSLTcpClient : public TcpClient
	{
	private:
		string m_caCertPath;
		bool m_isVerify;

		SSL_CTX* m_ssl_ctx;
		SSL* m_ssl;
	public:
		SSLTcpClient();
		~SSLTcpClient();

		void setCaCertPath(const string& certPath);
		void setCertificateVerify(bool on);

		const string& getCaCertPath()const;
		bool isCertificateVerify()const;

		int sendto(const string& sndbuf);
		int recvfrom(string& rcvbuf);

		void freeSSL();
	};
};