#include "ssl_tcpclient.hpp"
#include "topconn_error.hpp"

#define maxBufSize      8192

namespace topconn
{
	SSLTcpClient::SSLTcpClient()
		: m_ssl_ctx(NULL)
		, m_ssl(NULL)
		, m_isVerify(false)
	{
	}

	SSLTcpClient::~SSLTcpClient()
	{
		freeSSL();
	}

	void SSLTcpClient::freeSSL()
	{
		SSLhelper::SSLClose(&m_ssl_ctx, &m_ssl);
	}

	void SSLTcpClient::setCaCertPath(const string& certPath)
	{
		m_caCertPath = certPath;
	}

	void SSLTcpClient::setCertificateVerify(bool on)
	{
		m_isVerify = on;
	}

	const string& SSLTcpClient::getCaCertPath()const
	{
		return m_caCertPath;
	}

	bool SSLTcpClient::isCertificateVerify()const
	{
		return m_isVerify;
	}

	int SSLTcpClient::sendto(const string& sndbuf)
	{
		int iRet = 0;
		char sError[80] = { 0 };

		string ssndBuf(sndbuf);
		if (m_sockFd < 0)
		{
			return INVALID_SOCKET;
		}
		freeSSL();

		/**
		 * 初始化SSL
		 */
		int iOption = SSL_OPTION_CLIENT | SSL_OPTION_NOCERT;
		m_ssl_ctx = SSLhelper::SSLInitCtx(
			m_caCertPath.c_str(),
			NULL,
			NULL,
			NULL,
			iOption,
			sError);
		if (m_ssl_ctx == NULL)
		{
			return SSL_INIT_ERROR;
		}

		/**
		 * 连接SSL
		 */
		iOption = m_isVerify ? SSL_OPTION_CHKCERT : 0;
		m_ssl = SSLhelper::SSLConnect(m_ssl_ctx, m_sockFd, iOption, sError);
		if (m_ssl == NULL)
		{
			freeSSL();
			return SSL_CONNECT_ERROR;
		}

		 /**
         * 发送数据
         */		
        if(SSLhelper::SSLWriteN(ssndBuf.c_str(), ssndBuf.length(), m_ssl) == 0)
        {
			freeSSL();
            return SSL_SEND_ERROR;
        }

		/**
		 * 设置超时时间
		 */
		struct timeval timeout = { 0,0 };
		timeout.tv_usec = m_maxTimeout;
		timeout.tv_sec = 0;
		if (::setsockopt(m_sockFd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1)
		{
			freeSSL();
			return SOCKETOPT_ERROR;
		}
		return 0;
	}

	int SSLTcpClient::recvfrom(string& rcvbuf)
	{
		int iRet = 0;
		char sError[80] = { 0 };

		if (m_sockFd < 0)
		{
			return INVALID_SOCKET;
		}

		if (!m_ssl)
		{
			return INVALID_HANDLE;
		}

		/**
		 * 读取报文头
		 */
		int nReadLen = 0;
		char sHeadBuf[256] = { 0 };
		uint32_t nRcvMesgLen = maxBufSize;
		char* psRcvBuf = nullptr;
		char sRcvBuf[maxBufSize] = { 0 };
		if (m_length > 0)
		{
			if (SSLhelper::SSLReadN(sHeadBuf, m_length, &nReadLen, m_ssl) <= 0)
			{
				freeSSL();
				if (errno == EAGAIN)
				{
					return SSL_TIMEOUT_ERROR;
				}
				return SSL_RECV_ERROR;
			}
			nRcvMesgLen = atoi(sHeadBuf);
		}
		if (nRcvMesgLen > sizeof(sRcvBuf))
		{
			psRcvBuf = (char*)calloc(nRcvMesgLen + 1, sizeof(char));
			if (!psRcvBuf)
			{
				freeSSL();
				return ALLOC_ERROR;
			}
		}

		/**
		 * 读取报文体
		 */
		nReadLen = 0;
		if (SSLhelper::SSLReadN(psRcvBuf, nRcvMesgLen, &nReadLen, m_ssl) <= 0)
		{
			freeSSL();
			if (nRcvMesgLen > sizeof(sRcvBuf))
			{
				free(psRcvBuf);
			}
			if (errno == EAGAIN)
			{
				return SSL_TIMEOUT_ERROR;
			}
			return SSL_RECV_ERROR;
		}
		rcvbuf = psRcvBuf;

		/**
		 * 关闭SSL
		 */
		if (nRcvMesgLen > sizeof(sRcvBuf))
		{
			free(psRcvBuf);
		}
		freeSSL();
		return SUCESSED;
	}
};