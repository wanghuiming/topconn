#pragma once
#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_exception.hpp"
#include "topconn_error.hpp"
#include "ssl_tcpclient.hpp"

namespace topconn
{
	class SSLTcpConnector : public Connector
	{
	protected:
		SSLTcpClient m_client;
	public:
		SSLTcpConnector(const TopConnResource& resource) : Connector(resource) {};
		void initConnect(ProtocalCfg& cfg);
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse, const string& atx);	
	};
};