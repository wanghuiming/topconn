#include "ssl_tcpconnector.hpp"
#include "topconn_exception.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void SSLTcpConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string host = cfg.get("host");
			string port = cfg.get("port");
			if (host.empty() || port.empty())
			{
				TLOG_ERROR("远程服务器地址或端口不能为空");
				throw ConnException("远程服务器地址或端口不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "port", 1024, 65535);
			ConnectorHelper::checkIntRange(cfg, "maxRecvTimeout", 1, 300);
			ConnectorHelper::checkIntRange(cfg, "maxConnTimeout", 1, 30);
			ConnectorHelper::checkIntRange(cfg, "length", 2, 10);
			ConnectorHelper::checkBooleanValue(cfg, "isBlock");
			ConnectorHelper::checkBooleanValue(cfg, "isIPV6");
			ConnectorHelper::checkBooleanValue(cfg, "isKeepalive");
		}
	}

	void SSLTcpConnector::initConnect(ProtocalCfg& cfg)
	{
		string host = ConnectorHelper::getValue(cfg, "host");
		string port = ConnectorHelper::getValue(cfg, "port");
		string maxRecvTimeout = ConnectorHelper::getValue(cfg, "maxRecvTimeout", "30");
		string maxConnTimeout = ConnectorHelper::getValue(cfg, "maxConnTimeout", "5");
		string isBlock = ConnectorHelper::getValue(cfg, "isBlock", "true");
		string isIPV6 = ConnectorHelper::getValue(cfg, "isIPV6", "false");
		string length = ConnectorHelper::getValue(cfg, "length", "0");
		string isVerify = ConnectorHelper::getValue(cfg, "isVerify", "false");
		string ca_cert_path = ConnectorHelper::getValue(cfg, "ca_cert_path");

		m_client.setAddr(host, topconn_string::to_int<string>(port));
		m_client.setMaxTimeout(topconn_string::to_int<string>(maxRecvTimeout));
		m_client.setMaxConnTimeout(topconn_string::to_int<string>(maxConnTimeout));
		m_client.setLength(topconn_string::to_int<string>(length));

		if (!isBlock.empty() && strcasecmp(isBlock.c_str(), "false") == 0)
			m_client.setBlock(false);

		if (!isIPV6.empty() && strcasecmp(isIPV6.c_str(), "true") == 0)
			m_client.setIpv6(true);

		m_client.setCaCertPath(ca_cert_path);
		if (!isVerify.empty() && strcasecmp(isVerify.c_str(), "true") == 0)
			m_client.setCertificateVerify(true);
	}

	int  SSLTcpConnector::execute(ProtocalCfg& cfg, const string& request, string& resopnse, const string& atx)
	{
		int iRet = 0;

		TLOG_DEBUG("host=[%s] port=[%d],maxTimeout=[%d]",
			m_client.getAddr().c_str(), m_client.getPort(), m_client.getMaxTimeout());

		iRet = m_client.connect();
		if (iRet != SUCESSED)
		{
			TLOG_ERROR("executor error:%s", topconn_strerror(iRet));
			if (iRet == CONNECT_ERROR)
				throw ConnException("与远程SSL_Tcp服务器建立连接失败");
			else if (iRet == CONNECT_TIMEOUT)
				throw ConnectTimeoutException("与远程SSL_Tcp服务器建立连接超时");
			else
				throw ConnException("与远程SSL_Tcp服务器建立连接异常");
		}
		TLOG_TRACE("连接远程服务器成功");

		iRet = m_client.sendto(request);
		if (iRet != SUCESSED)
		{
			TLOG_ERROR("executor error:%s", topconn_strerror(iRet));
			if (iRet == SEND_ERROR)
				throw ConnException("发送数据至远程SSL_Tcp服务器失败");
			else
				throw ConnException("发送数据至远程SSL_Tcp服务器异常");
		}
		TLOG_TRACE("发送数据至远程SSL_Tcp服务器成功");

		iRet = m_client.recvfrom(resopnse);
		if (iRet != SUCESSED)
		{
			TLOG_ERROR("executor error:%s", topconn_strerror(iRet));
			if (iRet == RECV_TIMEOUT)
				throw RecvTimeoutException("接收远程Tcp服务器数据超时");
			else
				throw ConnException("接收远程SSL_Tcp服务器数据异常");
		}
		TLOG_TRACE("接收远程SSL_Tcp服务器数据成功");
		return SUCESSED;
	}
};