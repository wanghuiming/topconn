/**
 * @file ssl_wrap.h
 * @author wanghm
 * @brief 
 * @version 0.1
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/pkcs7.h>
#include <openssl/pkcs12.h>
#include <openssl/err.h>
#include <openssl/ssl.h>
#include <openssl/dh.h>

#define SSL_TINY_BUF  64
#define SSL_MINI_BUF  256
#define SSL_LARGE_BUF 1024
#define SSL_HUGE_BUF  10240

#define SSL_OPTION_RTIMEOUT   0x00000001
#define SSL_OPTION_STIMEOUT   0x00000002
#define SSL_OPTION_KEEPALIVE  0x00000004
#define SSL_OPTION_LINGER     0x00000008
#define SSL_OPTION_USESSL     0x00000010
#define SSL_OPTION_CHKCERT    0x00000020
#define SSL_OPTION_CLIENT     0x00000040
#define SSL_OPTION_NOCERT     0x00000080

#define SSLSTR_ERROR ERR_reason_error_string(ERR_get_error())

/*********************SSL*****************
*SSL (Secure Sockets Layer）安全套接层。
*server:
*	SSLInitCtx() -> SSLAccept() -> SSLReadN() -> SSLWriteN() -> SSLClose();
*	SSLAccept在于accept后调用。
*client:
*	SSLInitCtx() -> SSLConnect() -> SSLReadN() -> SSLWriteN() -> SSLClose();	
*	SSLConnect在于connect后调用。
*****************************************/
class SSLhelper
{
    SSLhelper() = default;
    ~SSLhelper() = default;
public:	
    /**
     * @brief 初始化SSL
     * 
     * @param psCaFile 
     * @param psCertFile 
     * @param psKeyFile 
     * @param psKeyPass 
     * @param iOption  
     * @param psError 
     * @return SSL_CTX* 
     */
	static SSL_CTX* SSLInitCtx(
		const char* psCaFile, 
		const char* psCertFile, 
		const char* psKeyFile, 
		char *psKeyPass, 
		int iOption,
		char* psError);

    /**
     * @brief SSLAccept
     * 
     * @param ctx 
     * @param sockFd 
     * @param iOption 
     * @param psError 
     * @return SSL* 
     */
	static SSL* SSLAccept(SSL_CTX* ctx,int sockFd,int iOption,char* psError);
	
    /**
     * @brief SSLConnect
     * 
     * @param ctx 
     * @param sockFd 
     * @param iOption 
     * @param psError 
     * @return SSL* 
     */
	static SSL* SSLConnect(SSL_CTX* ctx,int sockFd,int iOption,char* psError);
	
    /**
     * @brief SSLCheckCert
     * 
     * @param ssl 
     * @param psCN 
     * @param iOption 
     * @param psError 
     * @return int 
     */
	static int  SSLCheckCert(SSL* ssl, char *psCN, int iOption,char* psError);
	
    /**
     * @brief SSLReadN
     * 
     * @param pBuf 
     * @param iLen 
     * @param piReadLen 
     * @param ssl 
     * @return int 
     */
	static int  SSLReadN(void *pBuf, int iLen, int *piReadLen, SSL *ssl);

    /**
     * @brief SSLWriteN
     * 
     * @param pBuf 
     * @param iLen 
     * @param ssl 
     * @return int 
     */
	static int  SSLWriteN(const void *pBuf, int iLen, SSL *ssl);		

    /**
     * @brief SSLClose
     * 
     * @param ctx 
     * @param ssl 
     * @return int 
     */
	static int  SSLClose(SSL_CTX** ctx, SSL** ssl);        	
};
