#include "ssl_util.hpp"

/**
 * @brief 初始化SSL
 * 
 * @param psCaFile 
 * @param psCertFile 
 * @param psKeyFile 
 * @param psKeyPass 
 * @param iOption 
 * @param psError 
 * @return SSL_CTX* 
 */
SSL_CTX* SSLhelper::SSLInitCtx(
	const char* psCaFile, 
	const char* psCertFile, 
	const char* psKeyFile, 
	char *psKeyPass, 
	int iOption,
	char* psError)
{
    const SSL_METHOD *meth = NULL;
    SSL_CTX *ctx = NULL;
    
    SSL_library_init();
    SSL_load_error_strings();
    
    if(iOption & SSL_OPTION_CLIENT) {
        meth = SSLv23_client_method();
    } else {
        meth = SSLv23_server_method();
    } 
    
    ctx = SSL_CTX_new(meth);
    if(ctx == NULL) {
        sprintf(psError, "SSL_CTX_new error");
        return NULL;
    }
    
	/*client端直接返回*/
    if(iOption & SSL_OPTION_NOCERT) {
        return ctx;
    }
    
	/*server端需要初始化证书与私钥*/
    /*设置私钥密码*/
    SSL_CTX_set_default_passwd_cb_userdata(ctx, psKeyPass);
    
    /**
	 * @brief type 密钥文件的类型
	 * 	 SSL_FILETYPE_ASN1：ASN.1格式又称DER格式。
         SSL_FILETYPE_PEM：PEM格式，DER格式经过base64计算后的格式。
	 */

    /* 加载自己的证书    */
    if (SSL_CTX_use_certificate_file(ctx, psCertFile, SSL_FILETYPE_PEM) <= 0) {
        sprintf(psError, "SSL_CTX_use_certificate_file[%s] error", psCertFile);
        SSL_CTX_free(ctx);
        return NULL;
    }
    
    /* 加载自己的密钥文件 */
    if(SSL_CTX_use_PrivateKey_file(ctx, psKeyFile, SSL_FILETYPE_PEM) <= 0) {
        sprintf(psError, "SSL_CTX_use_PrivateKey_file[%s] error", psKeyFile);
        SSL_CTX_free(ctx);
        return NULL;
    }
    
    /* 验证验证密钥与证书是否配对 */
    if (!SSL_CTX_check_private_key(ctx)) {
        sprintf(psError, "SSL_CTX_check_private_key error");
        SSL_CTX_free(ctx);
        return NULL;
    }
    
    /* 设置信任证书 (ca证书)*/
    if(SSL_CTX_load_verify_locations(ctx, psCaFile, NULL) <= 0) {
        sprintf(psError, "SSL_CTX_load_verify_locations error");
        SSL_CTX_free(ctx);
        return NULL;
    }
/*    
#if (OPENSSL_VERSION_NUMBER < 0x00905100L)
    SSL_CTX_set_verify_depth(ctx, 1);
#endif
*/    
    return ctx;
}

int SSLhelper::SSLCheckCert(SSL* ssl, char *psCN, int iOption,char* psError)
{
    X509 *peer;
    char peer_CN[SSL_MINI_BUF];
    
    memset(peer_CN, 0, sizeof(peer_CN));
    
    if(SSL_get_verify_result(ssl) != X509_V_OK) {
        sprintf(psError, "SSL_get_verify_result error");
        return -1;
    }
    
    if(iOption & SSL_OPTION_CHKCERT) {
        peer = SSL_get_peer_certificate(ssl);
        
        X509_NAME_get_text_by_NID(X509_get_subject_name(peer),
            NID_commonName, peer_CN, SSL_MINI_BUF);
        if(strcasecmp(peer_CN, psCN)) {
            sprintf(psError, "X509_NAME_get_text_by_NID [%s] error", psCN);
            return -2;
        }
    }
    return 0;
}

int SSLhelper::SSLReadN(void *pBuf, int iLen, int* piReadLen, SSL *ssl)
{
    int iReadLen = 0;
    int iRet = 0;
    
    while(iReadLen < iLen) {
        iRet = SSL_read(ssl, (char*)pBuf+iReadLen, iLen - iReadLen);
        if(-1 == iRet) {
			int err = errno;
            fprintf(stderr, "SSL_read error len[%d] readlen[%d]\n", iLen, iReadLen);
			errno = err;
            return -1;
        } else if(0 == iRet) {
            return 0;
        }
        iReadLen += iRet;
        *piReadLen = iReadLen;
    }
    return iReadLen;
}

int SSLhelper::SSLWriteN(const void *pBuf, int iLen, SSL *ssl)
{
    int iWriteLen = 0;
    int iRet = 0;
    
    while(iWriteLen < iLen) {
        iRet = SSL_write(ssl, (const char*)pBuf+iWriteLen, iLen - iWriteLen);
        if(iRet <= 0) {
			int err = errno;
            fprintf(stderr, "SSL_write error len[%d] writelen[%d]\n", iLen, iWriteLen);
			errno = err;
            return iWriteLen;
        }
        iWriteLen += iRet;
    }
    return iWriteLen;
}

SSL* SSLhelper::SSLAccept(SSL_CTX* ctx,int sockFd,int iOption,char* psError)
{
	int iRet = 0;
	char  sTmp[80] = {0};
	SSL* ssl = NULL;
	if(ctx == NULL){
		sprintf(psError, " SSL_CTX handle is NULL");
		return NULL;
	}	

	ssl = SSL_new(ctx); 
	if(ssl == NULL){
		sprintf(psError, "SSL_new error[%s]", SSLSTR_ERROR);
		return ssl;
	}
	SSL_set_fd(ssl, sockFd);

	iRet = SSL_accept(ssl);
	if (iRet == -1) {
		sprintf(psError, "SSL_accept error[%s]", SSLSTR_ERROR);
		SSL_free(ssl);
		ssl = NULL;
		return ssl;
	}	 

	iRet = SSLCheckCert(ssl, NULL, iOption,sTmp);
	if(iRet != 0) {
		sprintf(psError, "SSLCheckCert error[%s]:%s", SSLSTR_ERROR,sTmp);
		/* 释放 SSL */
		SSL_free(ssl);
		ssl = NULL;
		return ssl;
	}
	return ssl;
}

SSL* SSLhelper::SSLConnect(SSL_CTX* ctx,int sockFd,int iOption,char* psError)
{
	int iRet = 0;
	char  sTmp[80] = {0};
	SSL* ssl = NULL;
	if(ctx == NULL){
		sprintf(psError, " SSL_CTX handle is NULL");
		return NULL;
	}	
	ssl = SSL_new(ctx); 
	if(ssl == NULL){
		sprintf(psError, "SSL_new error[%s]", SSLSTR_ERROR);
		return ssl;
	}
	SSL_set_fd(ssl, sockFd);

	iRet = SSL_connect(ssl);
	if (iRet == -1) {
		sprintf(psError, "SSL_connect error[%s]", SSLSTR_ERROR);
		SSL_free(ssl);
		ssl = NULL;
		return ssl;
	}
	iRet = SSLCheckCert(ssl, NULL, iOption,sTmp);
	if(iRet != 0) {
		sprintf(psError, "SSLCheckCert error[%s]", SSLSTR_ERROR);
		/* 释放 SSL */
		SSL_free(ssl);
		ssl = NULL;
		return ssl;
	}
	return ssl;	
}

int SSLhelper::SSLClose(SSL_CTX** ctx, SSL** ssl)
{
	if(*ssl) {
		SSL_free(*ssl);
		*ssl = NULL;
	}	
	if(*ctx) {
		SSL_CTX_free(*ctx);/* 释放 CTX */
		*ctx = NULL;
	}	
    return 0;
}