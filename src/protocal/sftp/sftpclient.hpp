#pragma once

#include <string>
#include <memory>
using namespace std;

#include <libssh2.h>
#include <libssh2_publickey.h>
#include <libssh2_sftp.h>

#include "protocal/tcp/tcpclient.hpp"

namespace topconn
{
	typedef enum {
		auth_pass = 0,
		auth_publickey
	}authType_t;

	class SftpClient : public TcpClient
	{
	private:
		LIBSSH2_SESSION* m_ssh2_session;
		LIBSSH2_SFTP* m_sftp_session;
		string m_username;
		string m_password;
		bool m_status;
		authType_t m_authType;
		string m_errors;
	public:
		SftpClient();
		SftpClient(const string& user,const string& pass);
		~SftpClient();

		const string& getErrors() const;

		void setAuth(authType_t authtype);
		void setUser(const string& user);
		void setPass(const string& pass);

		int connect();
		int disConnect();

		int putfile(const string& localFile,const string& remoteFile);
		int getfile(const string& remoteFile,const string& localFile);
	private:
		bool dir_exists(const string& remoteDir);
		int  check_remotepath(const string& remoteFile);
	};
};
