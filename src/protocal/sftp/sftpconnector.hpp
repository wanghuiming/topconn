#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"

#include "sftpclient.hpp"

namespace topconn
{

	class SftpConnector : public Connector
	{
	private:		
		SftpClient m_client;
	public:
		SftpConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse, const string& atx);
		void initConnect(ProtocalCfg& cfg);
	};
};