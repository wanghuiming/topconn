#include "sftpclient.hpp"

#include <fstream>
#include <vector>

#include "topconn_string.hpp"
#include "protocal/tcp/sockutil.h"

namespace topconn
{
	static std::streampos getFileSize(const std::string& fileName) 
	{
		std::ifstream inFile;

		inFile.open(fileName, std::ios::in);
		if (!inFile.is_open()) 
		{	
			return -1;
		}
		inFile.seekg(0, std::ios_base::end);
		std::streampos fileSize = inFile.tellg();
		inFile.seekg(0, std::ios_base::beg);
		inFile.close();
		return fileSize;
	}

	static std::vector<string> split_path(const string& c, const string& delim="/")
	{
		std::vector<string> rs_strings;
		if (c.size() == 0)
		{
			return rs_strings;
		}

		string sstring(c);
		string::size_type pos;
		if (sstring.compare(sstring.size() - delim.size(), delim.size(), delim) != 0)
		{
			sstring += delim;
		}

		uint32_t size = sstring.size();
		for (uint32_t i = 0; i < size; i++)
		{
			pos = sstring.find(delim, i);
			if (pos < size)
			{
				string s = sstring.substr(i, pos - i);
				rs_strings.push_back(s);
				i = pos + delim.size() - 1;
			}
		}
		return rs_strings;
	}

	SftpClient::SftpClient()
		: m_ssh2_session(nullptr)
		, m_sftp_session(nullptr)
		, m_status(false)
		, m_authType(auth_pass)
	{
		char sError[256] = { 0 };
		int rc = libssh2_init(0);
		if (rc != 0) {
			topconn_string::format(m_errors, "libssh2 init fail:%d",rc);
		}
	}

	SftpClient::SftpClient(const string& user, const string& pass)
		: m_ssh2_session(nullptr)
		, m_sftp_session(nullptr)
		, m_status(false)
		, m_username(user)
		, m_password(pass)
		, m_authType(auth_pass)
	{
		char sError[256] = { 0 };
		int rc = libssh2_init(0);
		if (rc != 0) {
			topconn_string::format(m_errors, "libssh2 init fail:%d", rc);
		}
	}

	SftpClient::~SftpClient() 
	{
		disConnect();
		libssh2_exit();
	}

	const string& SftpClient::getErrors() const
	{
		return m_errors;
	}

	void SftpClient::setAuth(authType_t authtype)
	{
		m_authType = authtype;
	}

	void SftpClient::setUser(const string& user)
	{
		m_username = user;
	}

	void SftpClient::setPass(const string& pass)
	{
		m_password = pass;
	}

	int SftpClient::connect()
	{
		int rc = 0;
		if (m_status)
		{
			return 0;
		}

		disConnect();
		if ((rc = TcpClient::connect()) != 0)
		{
			topconn_string::format(m_errors, "connect to server fail:%d", rc);
			return -1;
		}

		//建立ssh通讯session
		m_ssh2_session = libssh2_session_init();
		if (!m_ssh2_session)
		{
			topconn_string::format(m_errors, "libssh2_session_init error");
			return -1;
		}

		libssh2_session_set_blocking(m_ssh2_session, 1);

		//ssh交换密钥等操作
		rc = libssh2_session_handshake(m_ssh2_session, m_sockFd);
		if (rc != 0) {
			topconn_string::format(m_errors, "libssh2_session_handshake error:%d",rc);
			return -1;
		}

		//密钥认证
		const char* fingerprint = libssh2_hostkey_hash(m_ssh2_session, LIBSSH2_HOSTKEY_HASH_SHA1);
#if _DEBUG_
		fprintf(stdout, "Fingerprint: ");
		for (int i = 0; i < 20; i++) {
			fprintf(stdout, "%02X ", (unsigned char)fingerprint[i]);
		}
		fprintf(stdout, "\n");
#endif

		//检查服务器可用的登录方式
		char* userauthlist = libssh2_userauth_list(m_ssh2_session, m_username.c_str(), m_username.length());
#if _DEBUG_
		fprintf(stdout, "userauthlist=%s\n", userauthlist);
#endif

		if (m_authType == auth_pass)
		{
			if (strstr(userauthlist, "password") == NULL)
			{
				topconn_string::format(m_errors, "libssh2 server not support username/password login");
				return -1;
			}
			if (libssh2_userauth_password(m_ssh2_session, m_username.c_str(), m_password.c_str()))
			{
				topconn_string::format(m_errors, "libssh2 login fail with username[%s] password[%s]", m_username.c_str(), m_password.c_str());
				return -1;
			}
		}
		else if (m_authType == auth_publickey)
		{
			char sPubkey[256] = { 0 };
			char sPrivkey[256] = { 0 };
			sprintf(sPubkey, "%s/.ssh/id_rsa.pub", getenv("HOME"));
			sprintf(sPrivkey, "%s/.ssh/id_rsa.pub", getenv("HOME"));

			if (strstr(userauthlist, "publickey") == NULL)
			{
				topconn_string::format(m_errors, "libssh2 server not support publickey login");
				return -1;
			}
			if ( libssh2_userauth_publickey_fromfile(m_ssh2_session, m_username.c_str(),sPubkey, sPrivkey,m_password.c_str()) ) 
			{
				topconn_string::format(m_errors, "Authentication by public key failed");
				return -1;
			}
		}

		m_sftp_session = libssh2_sftp_init(m_ssh2_session);
		if (!m_sftp_session)
		{
			topconn_string::format(m_errors, "libssh2 sftp init fail");
			return -1;
		}
		m_status = true;
		return 0;
	}

	int SftpClient::disConnect()
	{
		if (m_sftp_session)
		{
			libssh2_sftp_shutdown(m_sftp_session);
			m_sftp_session = nullptr;
		}

		if (m_ssh2_session)
		{
			libssh2_session_disconnect(m_ssh2_session, "Normal Shutdown");
			libssh2_session_free(m_ssh2_session);
			m_ssh2_session = nullptr;
		}

		TcpClient::disConnect();
		m_status = false;
	}

	int SftpClient::putfile(const string& localFile, const string& remoteFile)
	{
		if (!m_status)
		{
			topconn_string::format(m_errors, "Unconnected to server");
			return -1;
		}

		int rc = 0;
		ifstream ifs(localFile, std::ifstream::binary);
		if (!ifs.is_open())
		{
			topconn_string::format(m_errors, "open file [%s] error:%s", localFile.c_str(),strerror(errno));
			return -1;
		}

		size_t filesize = getFileSize(localFile);
		if (filesize < 0)
		{
			topconn_string::format(m_errors, "get filesize error:%s", strerror(errno));
			return -1;
		}

		if (check_remotepath(remoteFile) != 0 )
		{
			return -1;
		}

		LIBSSH2_SFTP_HANDLE* sftp_handle = libssh2_sftp_open(
			m_sftp_session, 
			remoteFile.c_str(), 
			LIBSSH2_FXF_WRITE | LIBSSH2_FXF_CREAT | LIBSSH2_FXF_TRUNC,
			LIBSSH2_SFTP_S_IRUSR | LIBSSH2_SFTP_S_IWUSR | LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IROTH);
		if (!sftp_handle)
		{
			topconn_string::format(m_errors, "libssh2 remote file open fail [%s]", remoteFile.c_str());
			return -1;
		}

		char* pbuf = new char[filesize+1];
		memset(pbuf,0x00, filesize+1);
		ifs.read(pbuf, filesize);

		size_t nWrite = filesize;
		size_t nPos = 0;
		do
		{
			rc = libssh2_sftp_write(sftp_handle, pbuf+ nPos, filesize - nPos);
			if (rc < 0)
				break;
			nPos += rc;
			nWrite -= rc;
		} while (nWrite);
		delete []pbuf;

		libssh2_sftp_close(sftp_handle);
		return rc;
	}

	int SftpClient::getfile(const string& remoteFile, const string& localFile)
	{
		if (!m_status)
		{
			topconn_string::format(m_errors, "Unconnected to server");
			return -1;
		}

		int rc = 0;
		char buffer[40960] = { 0 };

		ofstream ofs(localFile);
		if (!ofs.is_open())
		{
			topconn_string::format(m_errors, "open file [%s] error:%s", localFile.c_str(), strerror(errno));
			return -1;
		}

		LIBSSH2_SFTP_HANDLE* sftp_handle = libssh2_sftp_open(
			m_sftp_session, 
			remoteFile.c_str(), 
			LIBSSH2_FXF_READ, 
			0);
		if (!sftp_handle)
		{
			topconn_string::format(m_errors, "libssh2_sftp_open remotefile[%s] error", remoteFile.c_str());
			return -1;
		}
		
		while (0 < (rc = libssh2_sftp_read(sftp_handle, buffer, sizeof(buffer))))
		{
			ofs << buffer;
			memset(buffer, 0, sizeof(buffer));
		}
		libssh2_sftp_close(sftp_handle);
		return rc;
	}

	bool SftpClient::dir_exists(const string& remoteDir)
	{
		bool rc = true;
		LIBSSH2_SFTP_HANDLE*  sftp_dir_handle = libssh2_sftp_opendir(
			m_sftp_session, remoteDir.c_str());
		if (!sftp_dir_handle)
		{
			rc = false;
			return rc;
		}
		libssh2_sftp_closedir(sftp_dir_handle);
		return rc;
	}

	int  SftpClient::check_remotepath(const string& remoteFile)
	{
		string path(remoteFile);
		string currentPath;

		vector<string> vPath = split_path(path);
		for (int i=0; i < vPath.size()-1; i++)
		{
			currentPath.append("/").append(vPath[i]);
			if ( !dir_exists(currentPath) )
			{
				int rc = libssh2_sftp_mkdir(
					m_sftp_session, 
					currentPath.c_str(),
					LIBSSH2_SFTP_S_IRWXU |
					LIBSSH2_SFTP_S_IRGRP | LIBSSH2_SFTP_S_IXGRP |
					LIBSSH2_SFTP_S_IROTH | LIBSSH2_SFTP_S_IXOTH);
				if (rc != 0) 
				{
					topconn_string::format(m_errors, "libssh2_sftp_mkdir error:%d", rc);
					return -1;
				}
			}
		}
		return 0;
	}

};