#include "sftpconnector.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void SftpConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string host = cfg.get("host");
			string port = cfg.get("port");
			if (host.empty())
			{
				TLOG_ERROR("远程主机地址host不能为空");
				throw ConnException("远程主机地址host不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "port", 22, 65535);
			ConnectorHelper::checkValidRange(cfg, "authmode", {"password","publickey"});
		}
	}

	void SftpConnector::initConnect(ProtocalCfg& cfg)
	{
		string host = ConnectorHelper::getValue(cfg, "host");
		string port = ConnectorHelper::getValue(cfg, "port", "22");
		string user = ConnectorHelper::getValue(cfg, "user");
		string pass = ConnectorHelper::getValue(cfg, "pass");
		string auth = ConnectorHelper::getValue(cfg, "authmode","password");

		int nport = topconn_string::to_int<string>(port);

		m_client.setAddr(host, nport);
		m_client.setUser(user);
		m_client.setPass(pass);

		if (auth == "publickey")
		{
			m_client.setAuth(auth_publickey);
		}

		if (m_client.connect() != 0)
		{
			TLOG_ERROR("初始化客户端连接失败:%s", m_client.getErrors().c_str());
			throw ConnException("初始化客户端连接失败:"+ m_client.getErrors());
		}
	}

	int  SftpConnector::execute(ProtocalCfg& cfg, const string& request, string& response, const string& atx)
	{
		int iRet = 0;

		//上传或下载文件
		const string& localfile = request;
		const string& method = atx;
		string& remotefile = response;

		if (method == "upload")
			iRet = m_client.putfile(localfile, remotefile);
		else if (method == "download")
			iRet = m_client.getfile(remotefile, localfile);
		else
			throw ConnException("不合法文件上传或下载操作标志");
		if (iRet != 0)
			throw ConnException("调用sftp上传或下载文件失败:" + m_client.getErrors());
		return 0;
	}
};