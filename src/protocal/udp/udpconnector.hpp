#pragma once

#include "topconn_cfg.hpp"
#include "topconn_connector.hpp"
#include "topconn_error.hpp"
#include "udpclient.hpp"

namespace topconn
{
	class UdpConnector : public Connector
	{
	protected:
		UdpClient m_client;
	public:
		UdpConnector(const TopConnResource& resource) : Connector(resource) {};
		void checkElementCfgValid(AttributeableCfg& cfg);
		void initConnect(ProtocalCfg& cfg);
		int  execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx);
	};
};