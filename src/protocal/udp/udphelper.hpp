#pragma once

#include "protocal/tcp/sockutil.h"

namespace topconn
{
	class UdpHelper
	{
	public:
		static int  create(bool isBlock = false, bool isIPV6 = false);
	};
};