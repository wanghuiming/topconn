#pragma once

#include <errno.h>

#include "protocal/tcp/sockutil.h"
#include "protocal/tcp/tcpclient.hpp"

namespace topconn
{
    class UdpClient
    {
    private:
        int m_sockFd;
        string   m_addr;
        uint16_t m_port;
        uint16_t m_maxTimeout;
        bool m_isBlock;
        bool m_isIpv6;
    public:
        UdpClient();
        UdpClient(const string& addr, uint16_t port);
        UdpClient(const string& addr, uint16_t port, uint16_t timeout);
        ~UdpClient();

        void setAddr(const string& addr, uint16_t port);
        void setMaxTimeout(uint16_t maxTimeout);
        void setBlock(bool on);
        void setIpv6(bool on);

        const string& getAddr() const;
        uint16_t getPort() const;
        uint16_t getMaxTimeout() const;
        bool isBlock() const;
        bool isIpv6() const;

        void close();

        int create();
        int sendto(const string& sndbuf);
        int recvfrom(string& rcvbuf);
    };
};