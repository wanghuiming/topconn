#include "udpconnector.hpp"
#include "topconn_exception.hpp"
#include "topconn_helper.hpp"

namespace topconn
{
	void UdpConnector::checkElementCfgValid(AttributeableCfg& cfg)
	{
		if (cfg.getCfgType() == cfgtype_t::E_PROTOCAL)
		{
			string host = cfg.get("host");
			string port = cfg.get("port");
			if (host.empty() || port.empty())
			{
				TLOG_ERROR("远程服务器地址或端口不能为空");
				throw ConnException("远程服务器地址或端口不能为空");
			}
			ConnectorHelper::checkIntRange(cfg, "port", 1024, 65535);
			ConnectorHelper::checkIntRange(cfg, "maxRecvTimeout", 1, 300);
			ConnectorHelper::checkBooleanValue(cfg, "isBlock");
			ConnectorHelper::checkBooleanValue(cfg, "isIPV6");
		}
	}

	int  UdpConnector::execute(ProtocalCfg& cfg, const string& request, string& resopnse,const string& atx)
	{
		int iRet = 0;
		
		iRet = m_client.create();
		if (iRet != SUCESSED)
		{
			throw ConnException("创建套接字失败:"+string(strerror(errno)));
		}

		TLOG_DEBUG("开始发送数据给远程服务器buffer=[%s]", request.c_str());
		iRet = m_client.sendto(request);
		if (iRet != SUCESSED)
		{
			if (iRet == SEND_ERROR)
				throw ConnException("发送数据至远程Udp服务器失败");
			else
				throw ConnException("发送数据至远程Udp服务器异常");
		}
		TLOG_TRACE("发送数据至远程Udp服务器成功");

		iRet = m_client.recvfrom(resopnse);
		if (iRet != SUCESSED)
		{
			if (iRet == RECV_TIMEOUT)
				throw RecvTimeoutException("接收远程Udp服务器数据超时");
			else
				throw ConnException("接收远程Udp服务器数据异常");
		}
		TLOG_TRACE("接收远程Udp服务器数据成功");
		return SUCESSED;
	}

	void UdpConnector::initConnect(ProtocalCfg& cfg)
	{
		string host = ConnectorHelper::getValue(cfg, "host");
		string port = ConnectorHelper::getValue(cfg, "port");
		string maxRecvTimeout = ConnectorHelper::getValue(cfg, "maxRecvTimeout", "30");
		string isBlock = ConnectorHelper::getValue(cfg, "isBlock", "true");
		string isIPV6 = ConnectorHelper::getValue(cfg, "isIPV6", "false");

		m_client.setAddr(host, topconn_string::to_int<string>(port));
		m_client.setMaxTimeout(topconn_string::to_int<string>(maxRecvTimeout));

		if (!isBlock.empty() && strcasecmp(isBlock.c_str(), "false") == 0)
		{
			m_client.setBlock(false);
		}

		if (!isIPV6.empty() && strcasecmp(isIPV6.c_str(), "true") == 0)
		{
			m_client.setIpv6(true);
		}
	}
};