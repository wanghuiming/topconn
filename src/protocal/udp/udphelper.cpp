#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>

#include "udphelper.hpp"

namespace topconn
{
	int UdpHelper::create(bool isBlock, bool isIPV6)
	{
		int option = SOCK_DGRAM | SOCK_CLOEXEC;
		if (!isBlock)
		{
			option |= SOCK_NONBLOCK;
		}
		int fd = socket(isIPV6 ? AF_INET6 : AF_INET, option, IPPROTO_UDP);
		if (fd < 0)
		{
			return fd;
		}
		return fd;
	}
};