#pragma once

#include <string>
#include <map>

using namespace std;

#include "pugixml/pugixml.hpp"
#include "topconn_cfg.hpp"

namespace topconn
{
	class ConnConfigParser
	{
	public:
		static void parseElementAttributes(pugi::xml_node& node, AttributeableCfg& cfg);
		static void parseRootCfg(pugi::xml_node& node, RootCfg& cfg);
		static void parseProtocalCfg(pugi::xml_node& node, ProtocalCfg& cfg);
	};
};