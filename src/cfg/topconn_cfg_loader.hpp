#pragma once

#include <string>
#include <fstream>
using namespace std;

#include "topconn_cfg.hpp"

namespace topconn
{
    using map_t = map<string, string>;

    class ConnResourceLoader
    {
    public:
        static TopConnResource& LoaderResource(const string& connPath,const string& connId,TopConnResource& resource);
        static TopConnResource& LoaderResource(const string& connType,map_t resource_maps, TopConnResource& resource);
    };

};