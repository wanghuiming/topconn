#pragma once

#include <string>
#include <fstream>
using namespace std;

#include "topconn_cfg.hpp"

namespace topconn
{
	struct ConnCfgInfo
	{
		string cfgpath;
		uint64_t modifyTime;
	};

	class ConnResourceReader
	{
	public:
		static void setPathLoadMode(bool on);

		ConnResourceReader()=default;
		~ConnResourceReader() = default;
		void loadConfigByFile(const string& pathFile);
		void loadConfigByString(const string& configData);
		inline pugi::xml_document& getDocument() { return m_document;};
	private:
		pugi::xml_document m_document;	
		static bool m_pathLoadMode;
		static std::map<string, string> m_resource;
		static std::map<string, long> m_lastTime;
		static uint64_t getMtime(const char* lpszFileName);
		static bool cacheContains(const string& fileName);
		static void put2Cache(const string& fileName, const long lastModified, const string& context);		
	};
};