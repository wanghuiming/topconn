
#include <iostream>
#include <time.h>

#ifndef WIN32
#include <sys/types.h>
#include <sys/stat.h>
#endif

#include "pugixml/pugixml.hpp"
#include "topconn_cross_platform.hpp"
#include "topconn_exception.hpp"
#include "topconn_logutil.hpp"

#include "topconn_cfg_reader.hpp"

namespace topconn
{
    const char* DateTimeFormat = "%Y%m%d%H%M%S";

    bool ConnResourceReader::m_pathLoadMode = false;
    std::map<string, string> ConnResourceReader::m_resource;
    std::map<string, long> ConnResourceReader::m_lastTime;

    struct xml_debug_writer : pugi::xml_writer  {
        string strXmlConn;
        string strXmlMsg;

        xml_debug_writer(const string& xmlConn)
            : strXmlConn(xmlConn)
        {};

        virtual void write(const void* data, size_t size)  {
            strXmlMsg += string(static_cast<const char*>(data), size);
        };
    };

    static string getParserStatus(pugi::xml_parse_status xmlParseStuts) {
        string xmlParseStatusInfo;
        switch (xmlParseStuts)  {
        case pugi::status_file_not_found:
            xmlParseStatusInfo = "file_not_found";
            break;
        case pugi::status_io_error:
            xmlParseStatusInfo = "io_error";
            break;
        case pugi::status_out_of_memory:
            xmlParseStatusInfo = "out_of_memory";
            break;
        case pugi::status_internal_error:
            xmlParseStatusInfo = "internal_error";
            break;
        case pugi::status_unrecognized_tag:
            xmlParseStatusInfo = "unrecognized_tag";
            break;
        case pugi::status_bad_pi:
            xmlParseStatusInfo = "bad_pi:document declaration/processing instruction error";
            break;
        case pugi::status_bad_comment:
            xmlParseStatusInfo = "bad_comment";
            break;
        case pugi::status_bad_cdata:
            xmlParseStatusInfo = "bad_cdata";
            break;
        case pugi::status_bad_doctype:
            xmlParseStatusInfo = "bad_doctype:document type declaration error";
            break;
        case pugi::status_bad_pcdata:
            xmlParseStatusInfo = "bad_pcdata:PCDATA section error";
            break;
        case pugi::status_bad_start_element:
            xmlParseStatusInfo = "bad_start_elemen";
            break;
        case pugi::status_bad_attribute:
            xmlParseStatusInfo = "bad_attribute:element attribute error";
            break;
        case pugi::status_bad_end_element:
            xmlParseStatusInfo = "bad_end_element:end element tag error";
            break;
        case pugi::status_end_element_mismatch:
            xmlParseStatusInfo = "end_element_mismatch";
            break;
        case pugi::status_append_invalid_root:
            xmlParseStatusInfo = "append_invalid_root";
            break;
        case pugi::status_no_document_element:
            xmlParseStatusInfo = "no_document_element";
            break;
        default:
            xmlParseStatusInfo = "ok";
        }
        return xmlParseStatusInfo;
    }

    void ConnResourceReader::setPathLoadMode(bool on)
    {
        ConnResourceReader::m_pathLoadMode = on;
    }

    uint64_t  ConnResourceReader::getMtime(const char* lpszFileName)
    {
        char lpszDataTime[40] = { 0 };
        if (access(lpszFileName, 0) < 0)  {
            return 0;
        }
        struct stat  f_stat;
        if (stat(lpszFileName, &f_stat) == -1) {
            char sError[1024] = { 0 };
            sprintf(sError, "Failed to get the specified profile property[%s][%s]", lpszFileName, strerror(errno));
            throw ConnException(sError);
        }
        tm  sttm;
        sttm = *localtime(&(f_stat.st_mtime));//���ʱ��
        strftime(lpszDataTime, 32, DateTimeFormat, &sttm); //"%Y%m%d%H%M%S"
        return atoll(lpszDataTime);
    }

    void ConnResourceReader::loadConfigByFile(const string& pathFile)
    {
        char sError[1024] = { 0 };
        string filePath;
        pugi::xml_parse_result xmlParseResult;

        const char* psEnvPktLoadMode = getenv("CONN_LOADMOE");
        if (psEnvPktLoadMode)  {
            ConnResourceReader::m_pathLoadMode = strcasecmp(psEnvPktLoadMode, "TRUE") ? false : true;
        }

        if (!ConnResourceReader::m_pathLoadMode)  {
            filePath.append(getenv(USERHOME)).append(1, PATH_DELIMITER).append(pathFile);
        } else  {
            filePath = pathFile;
        }

        if (access(filePath.c_str(), 0) != 0)  {
            sprintf(sError, "The specified profile [%s] was not found", filePath.c_str());
            TLOG_ERROR(sError);
            throw ConnException("The specified profile[" + filePath + "] was not found");
        }

#ifdef ENABLE_USE_CACHE
        uint64_t fileLastModifyTime = ConnResourceReader::getPktMtime(filePath.c_str());
        if (ConnResourceReader::cacheContains(filePath) && fileLastModifyTime == ConnResourceReader::m_lastTime[filePath]) {
            xmlParseResult = m_document.load_buffer(
                ConnResourceReader::m_resource[filePath].c_str(),
                ConnResourceReader::m_resource[filePath].size(),
                pugi::parse_default,
                pugi::encoding_auto);
            if (xmlParseResult.status != 0)  {
                sprintf(sError, "Failed to load the specified profile [%s],error reason[%s]",
                    filePath.c_str(), getParserStatus(xmlParseResult.status).c_str());
                throw ConnException(sError);
            }
        }
#endif
        xmlParseResult = m_document.load_file(filePath.c_str(), pugi::parse_default, pugi::encoding_auto);
        if (xmlParseResult.status != 0)  {
            sprintf(sError, "Failed to load the specified profile [%s],error reason[%s]",
                filePath.c_str(), getParserStatus(xmlParseResult.status).c_str());
            throw ConnException(sError);
        }

#ifdef ENABLE_USE_CACHE
        xml_debug_writer writer(BASENAME(filePath.c_str()));
        m_document.print(writer);
        ConnResourceReader::put2Cache(filePath, fileLastModifyTime, writer.strXmlMsg);
#endif
    }

    void ConnResourceReader::loadConfigByString(const string& configData)
    {
        char sError[1024] = { 0 };
        pugi::xml_parse_result xmlParseResult;
        xmlParseResult = m_document.load_buffer(configData.c_str(), pugi::parse_default, pugi::encoding_auto);
        if (xmlParseResult.status != 0) {
            sprintf(sError, "Failed to load the specified buffer [%s],error reason[%s]",
                configData.c_str(), getParserStatus(xmlParseResult.status).c_str());
            throw ConnException(sError);
        }
    }

    bool ConnResourceReader::cacheContains(const string& fileName)
    {
        auto iter = ConnResourceReader::m_resource.find(fileName);
        if (iter != ConnResourceReader::m_resource.end())  {
            return false;
        }
        return true;
    }

    void ConnResourceReader::put2Cache(const string& fileName, const long lastModifiedTime, const string& context)
    {
        ConnResourceReader::m_resource[fileName] = context;
        ConnResourceReader::m_lastTime[fileName] = lastModifiedTime;
    }
  
};

