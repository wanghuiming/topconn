#include "topconn_cfg_parser.hpp"

namespace topconn
{
	void ConnConfigParser::parseElementAttributes(pugi::xml_node& node, AttributeableCfg& cfg)
	{
		for (pugi::xml_attribute attr = node.first_attribute(); attr; attr = attr.next_attribute())
		{
			cfg.set(attr.name(), attr.value());
		}
	}

	void ConnConfigParser::parseRootCfg(pugi::xml_node& node, RootCfg& cfg)
	{
		cfg.setFormat(node.attribute("format").value());
		cfg.setEncoding(node.attribute("encoding").value());
	}

	void ConnConfigParser::parseProtocalCfg(pugi::xml_node& node, ProtocalCfg& cfg)
	{
		string node_name = node.name();
		cfg.setDesc(node.attribute("desc").value());
		cfg.setType(node.attribute("type").value());
		cfg.setId(node.attribute("id").value());
	}
};