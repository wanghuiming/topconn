#pragma once

#include <string>
#include <memory>
using namespace std;

#include "pugixml/pugixml.hpp"

#include "topconn_exception.hpp"
#include "topconn_cfg.hpp"

namespace topconn
{
	class Connector;
    typedef std::shared_ptr<Connector>  ConnectorPtr_t;

	class Connector
	{
	private:
		TopConnResource m_resource;
	public:
		/**
		 * 构造函数、析构函数
		 */
		Connector(const TopConnResource& resource) : m_resource(resource) {};
		virtual ~Connector() {};

		/**
		 * 执行通讯调用
		 */
		int doExecute(const string& request,string& resopnse,const string& atx);

		/**
		 * 执行通讯检查
		 */
		void doCheck();
	private:
		// 虚函数接口
		virtual void checkElementCfgValid(AttributeableCfg& cfg) = 0;
		virtual void initConnect(ProtocalCfg& cfg) = 0;
		virtual int  execute(ProtocalCfg& cfg,const string& request,string& resopnse,const string& atx) = 0;
	};
}
