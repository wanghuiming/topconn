#include "topconn_component.hpp"
#include "topconn_exception.hpp"
#include "topconn_processor.hpp"

#include "cfg/topconn_cfg_loader.hpp"
#include "cfg/topconn_cfg_reader.hpp"

#include "topconn_string.hpp"

namespace topconn
{
    static string _connPath = "connector.xml";

    void   ConnComponent::setConnLoadMode(bool mode)
    {
        ConnResourceReader::setPathLoadMode(mode);
    }

    void  ConnComponent::setConnPath(const string& connPath)
    {
        _connPath = connPath;
    }

    int ConnComponent::sendPack(const string& connectorId, const string& request, string& response,const string& atx)
    {
        return ConnProcessor::sendPack(connectorId, _connPath, request, response, atx);
    }

    int  ConnComponent::sendPack(const string& protocalType, std::map<string, string>& params, const string& request, string& response,const string& atx)
    {
        return ConnProcessor::sendPack(protocalType,params,request,response,atx);
    }
};