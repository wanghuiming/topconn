#include "topconn_connector.hpp"

namespace topconn
{
    /**
     * 执行通讯调用
     */
    int Connector::doExecute(const string& request,string& resopnse,const string& atx) {
        // 进行通讯节点属性检查
        checkElementCfgValid(m_resource.getProtocalCfg());
        // 初始化连接
        initConnect(m_resource.getProtocalCfg());
        // 执行通讯具体调用
        return execute(m_resource.getProtocalCfg(),request,resopnse,atx);
    }    

    /**
     * 执行通讯检查
     */
    void Connector::doCheck()
    {
        checkElementCfgValid(m_resource.getProtocalCfg());
    }
}
