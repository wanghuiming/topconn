#include "topconn_helper.hpp"

namespace topconn
{
    string ConnectorHelper::getValue(AttributeableCfg& t, const string& attr, const string& defVal)
    {
        string value = t.get(attr);
        return value.empty() && !defVal.empty() ? defVal : value;
    }

    void ConnectorHelper::checkValidRange(AttributeableCfg& e, const string& attr, std::initializer_list<std::string> allowValues)
    {
        string name = ConnectorHelper::getName(e);
        string value = e.get(attr);
        topconn_string::trim(value);
        if (!value.empty())
        {
            for (string allow : allowValues)
            {
                if (value == allow)
                {
                    return;
                }
            }
            TLOG_ERROR("节点字段[%s]属性[%s]值范围有误",name.c_str(),attr.c_str());
            throw  ConnException(string("节点字段[") + name + "]属性[" + attr + "]值范围有误");
        }
    }

    void ConnectorHelper::checkIntRange(AttributeableCfg& e, const string& attr, int min, int max)
    {
        string name = ConnectorHelper::getName(e);
        string value = e.get(attr);

        topconn_string::trim(value);

        if (!value.empty())
        {
            if (!topconn_string::isnumber(value))
            {
                TLOG_ERROR("节点字段[%s]属性[%s]值需要整数,应为数字", name.c_str(), attr.c_str());
                throw ConnException("节点字段[" + name + "]属性[" + attr + "]值需要整数,应为数字");
            }

            int val = topconn_string::to_int<string>(value);
            if (val < min || val > max)
            {
                throw ConnException("节点字段[" + name + "]属性[" + attr + "]值范围有误,应在" +
                    topconn_string::to_string<int>(min) + "~" + topconn_string::to_string<int>(max) + "之间");
            }
        }
    }

    void ConnectorHelper::checkBooleanValue(AttributeableCfg& e, const string& attr)
    {
        ConnectorHelper::checkValidRange(e, attr, { "true", "false" });
    }

    string ConnectorHelper::getName(AttributeableCfg& e)
    {
        if (e.getCfgType() == cfgtype_t::E_PROTOCAL)
        {
            return string("protocal");
        }
        return e.getName();
    }
};